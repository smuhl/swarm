// Descriptive statistics by cluster

use "C:\Users\ru11kahi\Dokumente Lokal\SWARM\Daten\Datensatz.dta", clear

cd "C:\Users\ru11kahi\Dokumente Lokal\SWARM\Daten\Analyse"
C:\Users\ru11kahi\Dokumente Lokal\SWARM\Daten\Analyse


//Socio-demographic
eststo: estpost tabstat gender age educ0 educ1 educ2 educ3 educ4 prof_own prof_publ prof_emp prof_wor prof_pen prof_home Stud_Nat_Ing , by (cluster3) stats(mean sd n) 
esttab using Descr_Socio.rtf, cells("gender(fmt(2)) age(fmt(2)) educ0(fmt(2)) educ1(fmt(2)) educ2(fmt(2)) educ3(fmt(2)) educ4(fmt(2)) prof_own(fmt(2)) prof_publ(fmt(2)) prof_emp(fmt(2)) prof_wor(fmt(2)) prof_pen(fmt(2)) prof_home(fmt(2)) Stud_Nat_Ing(fmt(2))")
eststo clear

//differences?
mean gender age educ0 educ1 educ2 educ3 educ4 prof_own prof_publ prof_emp prof_wor prof_pen prof_home Stud_Nat_Ing, over(cluster3)
estat sd
lincom [gender]1 - [gender]2
lincom [gender]1 - [gender]3
lincom [gender]1 - [gender]4
lincom [gender]2 - [gender]3
lincom [gender]2 - [gender]4
lincom [gender]3 - [gender]4

lincom [age]1 - [age]2
lincom [age]1 - [age]3
lincom [age]1 - [age]4
lincom [age]2 - [age]3
lincom [age]2 - [age]4
lincom [age]3 - [age]4

lincom [educ0]1 - [educ0]2
lincom [educ0]1 - [educ0]3
lincom [educ0]1 - [educ0]4
lincom [educ0]2 - [educ0]3
lincom [educ0]2 - [educ0]4
lincom [educ0]3 - [educ0]4

lincom [educ1]1 - [educ1]2
lincom [educ1]1 - [educ1]3
lincom [educ1]1 - [educ1]4
lincom [educ1]2 - [educ1]3
lincom [educ1]2 - [educ1]4
lincom [educ1]3 - [educ1]4

lincom [educ2]1 - [educ2]2
lincom [educ2]1 - [educ2]3
lincom [educ2]1 - [educ2]4
lincom [educ2]2 - [educ2]3
lincom [educ2]2 - [educ2]4
lincom [educ2]3 - [educ2]4

lincom [educ3]1 - [educ3]2
lincom [educ3]1 - [educ3]3
lincom [educ3]1 - [educ3]4
lincom [educ3]2 - [educ3]3
lincom [educ3]2 - [educ3]4
lincom [educ3]3 - [educ3]4

lincom [educ4]1 - [educ4]2
lincom [educ4]1 - [educ4]3
lincom [educ4]1 - [educ4]4
lincom [educ4]2 - [educ4]3
lincom [educ4]2 - [educ4]4
lincom [educ4]3 - [educ4]4

lincom [age]1 - [age]2
lincom [age]1 - [age]3
lincom [age]1 - [age]4
lincom [age]2 - [age]3
lincom [age]2 - [age]4
lincom [age]3 - [age]4

lincom [educ0]1 - [educ0]2
lincom [educ0]1 - [educ0]3
lincom [educ0]1 - [educ0]4
lincom [educ0]2 - [educ0]3
lincom [educ0]2 - [educ0]4
lincom [educ0]3 - [educ0]4

lincom [educ1]1 - [educ1]2
lincom [educ1]1 - [educ1]3
lincom [educ1]1 - [educ1]4
lincom [educ1]2 - [educ1]3
lincom [educ1]2 - [educ1]4
lincom [educ1]3 - [educ1]4

lincom [educ2]1 - [educ2]2
lincom [educ2]1 - [educ2]3
lincom [educ2]1 - [educ2]4
lincom [educ2]2 - [educ2]3
lincom [educ2]2 - [educ2]4
lincom [educ2]3 - [educ2]4

lincom [educ3]1 - [educ3]2
lincom [educ3]1 - [educ3]3
lincom [educ3]1 - [educ3]4
lincom [educ3]2 - [educ3]3
lincom [educ3]2 - [educ3]4
lincom [educ3]3 - [educ3]4

lincom [educ4]1 - [educ4]2
lincom [educ4]1 - [educ4]3
lincom [educ4]1 - [educ4]4
lincom [educ4]2 - [educ4]3
lincom [educ4]2 - [educ4]4
lincom [educ4]3 - [educ4]4

lincom [prof_own]1 - [prof_own]2
lincom [prof_own]1 - [prof_own]3
lincom [prof_own]1 - [prof_own]4
lincom [prof_own]2 - [prof_own]3
lincom [prof_own]2 - [prof_own]4
lincom [prof_own]3 - [prof_own]4

lincom [prof_publ]1 - [prof_publ]2
lincom [prof_publ]1 - [prof_publ]3
lincom [prof_publ]1 - [prof_publ]4
lincom [prof_publ]2 - [prof_publ]3
lincom [prof_publ]2 - [prof_publ]4
lincom [prof_publ]3 - [prof_publ]4

lincom [prof_emp]1 - [prof_emp]2
lincom [prof_emp]1 - [prof_emp]3
lincom [prof_emp]1 - [prof_emp]4
lincom [prof_emp]2 - [prof_emp]3
lincom [prof_emp]2 - [prof_emp]4
lincom [prof_emp]3 - [prof_emp]4

lincom [prof_wor]1 - [prof_wor]2
lincom [prof_wor]1 - [prof_wor]3
lincom [prof_wor]1 - [prof_wor]4
lincom [prof_wor]2 - [prof_wor]3
lincom [prof_wor]2 - [prof_wor]4
lincom [prof_wor]3 - [prof_wor]4

lincom [prof_pen]1 - [prof_pen]2
lincom [prof_pen]1 - [prof_pen]3
lincom [prof_pen]1 - [prof_pen]4
lincom [prof_pen]2 - [prof_pen]3
lincom [prof_pen]2 - [prof_pen]4
lincom [prof_pen]3 - [prof_pen]4

lincom [prof_home]1 - [prof_home]2
lincom [prof_home]1 - [prof_home]3
lincom [prof_home]1 - [prof_home]4
lincom [prof_home]2 - [prof_home]3
lincom [prof_home]2 - [prof_home]4
lincom [prof_home]3 - [prof_home]4

lincom [Stud_Nat_Ing]1 - [Stud_Nat_Ing]2
lincom [Stud_Nat_Ing]1 - [Stud_Nat_Ing]3
lincom [Stud_Nat_Ing]1 - [Stud_Nat_Ing]4
lincom [Stud_Nat_Ing]2 - [Stud_Nat_Ing]3
lincom [Stud_Nat_Ing]2 - [Stud_Nat_Ing]4
lincom [Stud_Nat_Ing]3 - [Stud_Nat_Ing]4


eststo: estpost tabstat married couple sep divorced widowed single nb_child nb_hh income lowinc middleinc highinc vhighinc , by (cluster3) stats(mean sd n)
esttab using Descr_Socio2.rtf, cells ("married(fmt(2)) couple(fmt(2)) sep(fmt(2)) divorced(fmt(2)) widowed(fmt(2)) single(fmt(2)) nb_child(fmt(2)) nb_hh(fmt(2)) income(fmt(2)) lowinc(fmt(2)) middleinc(fmt(2)) highinc(fmt(2)) vhighinc(fmt(2))")
eststo clear

//differences?
mean married couple sep divorced widowed single nb_child nb_hh income lowinc middleinc highinc vhighinc, over(cluster3)
estat sd

lincom [married]1 - [married]2
lincom [married]1 - [married]3
lincom [married]1 - [married]4
lincom [married]2 - [married]3
lincom [married]2 - [married]4
lincom [married]3 - [married]4

lincom [couple]1 - [couple]2
lincom [couple]1 - [couple]3
lincom [couple]1 - [couple]4
lincom [couple]2 - [couple]3
lincom [couple]2 - [couple]4
lincom [couple]3 - [couple]4

lincom [sep]1 - [sep]2
lincom [sep]1 - [sep]3
lincom [sep]1 - [sep]4
lincom [sep]2 - [sep]3
lincom [sep]2 - [sep]4
lincom [sep]3 - [sep]4

lincom [divorced]1 - [divorced]2
lincom [divorced]1 - [divorced]3
lincom [divorced]1 - [divorced]4
lincom [divorced]2 - [divorced]3
lincom [divorced]2 - [divorced]4
lincom [divorced]3 - [divorced]4

lincom [widowed]1 - [widowed]2
lincom [widowed]1 - [widowed]3
lincom [widowed]1 - [widowed]4
lincom [widowed]2 - [widowed]3
lincom [widowed]2 - [widowed]4
lincom [widowed]3 - [widowed]4

lincom [single]1 - [single]2
lincom [single]1 - [single]3
lincom [single]1 - [single]4
lincom [single]2 - [single]3
lincom [single]2 - [single]4
lincom [single]3 - [single]4

lincom [nb_child]1 - [nb_child]2
lincom [nb_child]1 - [nb_child]3
lincom [nb_child]1 - [nb_child]4
lincom [nb_child]2 - [nb_child]3
lincom [nb_child]2 - [nb_child]4
lincom [nb_child]3 - [nb_child]4

lincom [nb_hh]1 - [nb_hh]2
lincom [nb_hh]1 - [nb_hh]3
lincom [nb_hh]1 - [nb_hh]4
lincom [nb_hh]2 - [nb_hh]3
lincom [nb_hh]2 - [nb_hh]4
lincom [nb_hh]3 - [nb_hh]4

lincom [income]1 - [income]2
lincom [income]1 - [income]3
lincom [income]1 - [income]4
lincom [income]2 - [income]3
lincom [income]2 - [income]4
lincom [income]3 - [income]4

lincom [lowinc]1 - [lowinc]2
lincom [lowinc]1 - [lowinc]3
lincom [lowinc]1 - [lowinc]4
lincom [lowinc]2 - [lowinc]3
lincom [lowinc]2 - [lowinc]4
lincom [lowinc]3 - [lowinc]4

lincom [middleinc]1 - [middleinc]2
lincom [middleinc]1 - [middleinc]3
lincom [middleinc]1 - [middleinc]4
lincom [middleinc]2 - [middleinc]3
lincom [middleinc]2 - [middleinc]4
lincom [middleinc]3 - [middleinc]4

lincom [highinc]1 - [highinc]2
lincom [highinc]1 - [highinc]3
lincom [highinc]1 - [highinc]4
lincom [highinc]2 - [highinc]3
lincom [highinc]2 - [highinc]4
lincom [highinc]3 - [highinc]4

lincom [vhighinc]1 - [vhighinc]2
lincom [vhighinc]1 - [vhighinc]3
lincom [vhighinc]1 - [vhighinc]4
lincom [vhighinc]2 - [vhighinc]3
lincom [vhighinc]2 - [vhighinc]4
lincom [vhighinc]3 - [vhighinc]4


eststo: estpost tabstat m_sq ownshp Verbrauch_Strom tarif_smart tarif_smartonline tarif_grund tarif_grunddoppel tarif_franken tarif_pur tarif_stabil tarif_garant tarif_sph kaufkraftpeinwohner kaufkraftphh SumFFCraft, by (cluster3) stats(mean sd n)
esttab using Descr_Socio3.rtf, cells ("m_sq(fmt(2)) ownshp(fmt(2)) Verbrauch_Strom(fmt(2)) tarif_smart(fmt(2)) tarif_smartonline(fmt(2)) tarif_grund(fmt(2)) tarif_grunddoppel(fmt(2)) tarif_franken(fmt(2)) tarif_pur(fmt(2)) tarif_stabil(fmt(2)) tarif_garant(fmt(2)) tarif_sph(fmt(2)) kaufkraftpeinwohner(fmt(2)) kaufkraftphh(fmt(2)) SumFFCraft(fmt(2))")
eststo clear

mean m_sq ownshp Verbrauch_Strom tarif_smart tarif_smartonline tarif_grund tarif_grunddoppel tarif_franken tarif_pur tarif_stabil tarif_garant tarif_sph kaufkraftpeinwohner kaufkraftphh SumFFCraft, over(cluster3)
estat sd

lincom [m_sq]1 - [m_sq]2
lincom [m_sq]1 - [m_sq]3
lincom [m_sq]1 - [m_sq]4
lincom [m_sq]2 - [m_sq]3
lincom [m_sq]2 - [m_sq]4
lincom [m_sq]3 - [m_sq]4

lincom [ownshp]1 - [ownshp]2
lincom [ownshp]1 - [ownshp]3
lincom [ownshp]1 - [ownshp]4
lincom [ownshp]2 - [ownshp]3
lincom [ownshp]2 - [ownshp]4
lincom [ownshp]3 - [ownshp]4

lincom [Verbrauch_Strom]1 - [Verbrauch_Strom]2
lincom [Verbrauch_Strom]1 - [Verbrauch_Strom]3
lincom [Verbrauch_Strom]1 - [Verbrauch_Strom]4
lincom [Verbrauch_Strom]2 - [Verbrauch_Strom]3
lincom [Verbrauch_Strom]2 - [Verbrauch_Strom]4
lincom [Verbrauch_Strom]3 - [Verbrauch_Strom]4

lincom [tarif_smart]1 - [tarif_smart]2
lincom [tarif_smart]1 - [tarif_smart]3
lincom [tarif_smart]1 - [tarif_smart]4
lincom [tarif_smart]2 - [tarif_smart]3
lincom [tarif_smart]2 - [tarif_smart]4
lincom [tarif_smart]3 - [tarif_smart]4

lincom [tarif_smartonline]1 - [tarif_smartonline]2
lincom [tarif_smartonline]1 - [tarif_smartonline]3
lincom [tarif_smartonline]1 - [tarif_smartonline]4
lincom [tarif_smartonline]2 - [tarif_smartonline]3
lincom [tarif_smartonline]2 - [tarif_smartonline]4
lincom [tarif_smartonline]3 - [tarif_smartonline]4

lincom [tarif_grund]1 - [tarif_grund]2
lincom [tarif_grund]1 - [tarif_grund]3
lincom [tarif_grund]1 - [tarif_grund]4
lincom [tarif_grund]2 - [tarif_grund]3
lincom [tarif_grund]2 - [tarif_grund]4
lincom [tarif_grund]3 - [tarif_grund]4

lincom [tarif_grunddoppel]1 - [tarif_grunddoppel]2
lincom [tarif_grunddoppel]1 - [tarif_grunddoppel]3
lincom [tarif_grunddoppel]1 - [tarif_grunddoppel]4
lincom [tarif_grunddoppel]2 - [tarif_grunddoppel]3
lincom [tarif_grunddoppel]2 - [tarif_grunddoppel]4
lincom [tarif_grunddoppel]3 - [tarif_grunddoppel]4

lincom [tarif_franken]1 - [tarif_franken]2
lincom [tarif_franken]1 - [tarif_franken]3
lincom [tarif_franken]1 - [tarif_franken]4
lincom [tarif_franken]2 - [tarif_franken]3
lincom [tarif_franken]2 - [tarif_franken]4
lincom [tarif_franken]3 - [tarif_franken]4

lincom [tarif_pur]1 - [tarif_pur]2
lincom [tarif_pur]1 - [tarif_pur]3
lincom [tarif_pur]1 - [tarif_pur]4
lincom [tarif_pur]2 - [tarif_pur]3
lincom [tarif_pur]2 - [tarif_pur]4
lincom [tarif_pur]3 - [tarif_pur]4

lincom [tarif_stabil]1 - [tarif_stabil]2
lincom [tarif_stabil]1 - [tarif_stabil]3
lincom [tarif_stabil]1 - [tarif_stabil]4
lincom [tarif_stabil]2 - [tarif_stabil]3
lincom [tarif_stabil]2 - [tarif_stabil]4
lincom [tarif_stabil]3 - [tarif_stabil]4

lincom [tarif_garant]1 - [tarif_garant]2
lincom [tarif_garant]1 - [tarif_garant]3
lincom [tarif_garant]1 - [tarif_garant]4
lincom [tarif_garant]2 - [tarif_garant]3
lincom [tarif_garant]2 - [tarif_garant]4
lincom [tarif_garant]3 - [tarif_garant]4

lincom [tarif_sph]1 - [tarif_sph]2
lincom [tarif_sph]1 - [tarif_sph]3
lincom [tarif_sph]1 - [tarif_sph]4
lincom [tarif_sph]2 - [tarif_sph]3
lincom [tarif_sph]2 - [tarif_sph]4
lincom [tarif_sph]3 - [tarif_sph]4

lincom [kaufkraftpeinwohner]1 - [kaufkraftpeinwohner]2
lincom [kaufkraftpeinwohner]1 - [kaufkraftpeinwohner]3
lincom [kaufkraftpeinwohner]1 - [kaufkraftpeinwohner]4
lincom [kaufkraftpeinwohner]2 - [kaufkraftpeinwohner]3
lincom [kaufkraftpeinwohner]2 - [kaufkraftpeinwohner]4
lincom [kaufkraftpeinwohner]3 - [kaufkraftpeinwohner]4

lincom [kaufkraftphh]1 - [kaufkraftphh]2
lincom [kaufkraftphh]1 - [kaufkraftphh]3
lincom [kaufkraftphh]1 - [kaufkraftphh]4
lincom [kaufkraftphh]2 - [kaufkraftphh]3
lincom [kaufkraftphh]2 - [kaufkraftphh]4
lincom [kaufkraftphh]3 - [kaufkraftphh]4

lincom [SumFFCraft]1 - [SumFFCraft]2
lincom [SumFFCraft]1 - [SumFFCraft]3
lincom [SumFFCraft]1 - [SumFFCraft]4
lincom [SumFFCraft]2 - [SumFFCraft]3
lincom [SumFFCraft]2 - [SumFFCraft]4
lincom [SumFFCraft]3 - [SumFFCraft]4


//Risk
eststo: estpost tabstat riskgen risk_techno risk_fin risk_driv risk_sport risk_career risk_health, by (cluster3) stats(mean sd n)
esttab using Descr.rtf, cells ("riskgen(fmt(2)) risk_techno(fmt(2)) risk_fin(fmt(2)) risk_driv(fmt(2)) risk_sport(fmt(2)) risk_career(fmt(2)) risk_health(fmt(2))")
eststo clear

mean riskgen risk_techno risk_fin risk_driv risk_sport risk_career risk_health, over(cluster3)
estat sd

lincom [riskgen]1 - [riskgen]2
lincom [riskgen]1 - [riskgen]3
lincom [riskgen]1 - [riskgen]4
lincom [riskgen]2 - [riskgen]3
lincom [riskgen]2 - [riskgen]4
lincom [riskgen]3 - [riskgen]4

lincom [risk_techno]1 - [risk_techno]2
lincom [risk_techno]1 - [risk_techno]3
lincom [risk_techno]1 - [risk_techno]4
lincom [risk_techno]2 - [risk_techno]3
lincom [risk_techno]2 - [risk_techno]4
lincom [risk_techno]3 - [risk_techno]4

lincom [risk_fin]1 - [risk_fin]2
lincom [risk_fin]1 - [risk_fin]3
lincom [risk_fin]1 - [risk_fin]4
lincom [risk_fin]2 - [risk_fin]3
lincom [risk_fin]2 - [risk_fin]4
lincom [risk_fin]3 - [risk_fin]4

lincom [risk_driv]1 - [risk_driv]2
lincom [risk_driv]1 - [risk_driv]3
lincom [risk_driv]1 - [risk_driv]4
lincom [risk_driv]2 - [risk_driv]3
lincom [risk_driv]2 - [risk_driv]4
lincom [risk_driv]3 - [risk_driv]4

lincom [risk_sport]1 - [risk_sport]2
lincom [risk_sport]1 - [risk_sport]3
lincom [risk_sport]1 - [risk_sport]4
lincom [risk_sport]2 - [risk_sport]3
lincom [risk_sport]2 - [risk_sport]4
lincom [risk_sport]3 - [risk_sport]4

lincom [risk_career]1 - [risk_career]2
lincom [risk_career]1 - [risk_career]3
lincom [risk_career]1 - [risk_career]4
lincom [risk_career]2 - [risk_career]3
lincom [risk_career]2 - [risk_career]4
lincom [risk_career]3 - [risk_career]4

lincom [risk_health]1 - [risk_health]2
lincom [risk_health]1 - [risk_health]3
lincom [risk_health]1 - [risk_health]4
lincom [risk_health]2 - [risk_health]3
lincom [risk_health]2 - [risk_health]4
lincom [risk_health]3 - [risk_health]4


//Trust
eststo: estpost tabstat trust_allgmenschen trust_keinverlass trust_vorsichtmfremd trust_unternehmen trust_adsnotrust trust_neueinfoskritisch, by (cluster3) stats(mean sd n)
esttab using Descr_risk.rtf, cells ("trust_allgmenschen(fmt(2)) trust_keinverlass(fmt(2)) trust_vorsichtmfremd(fmt(2)) trust_unternehmen(fmt(2)) trust_adsnotrust(fmt(2)) trust_neueinfoskritisch(fmt(2))")
eststo clear

mean trust_allgmenschen trust_keinverlass trust_vorsichtmfremd trust_unternehmen trust_adsnotrust trust_neueinfoskritisch, over(cluster3)
estat sd

lincom [trust_allgmenschen]1 - [trust_allgmenschen]2
lincom [trust_allgmenschen]1 - [trust_allgmenschen]3
lincom [trust_allgmenschen]1 - [trust_allgmenschen]4
lincom [trust_allgmenschen]2 - [trust_allgmenschen]3
lincom [trust_allgmenschen]2 - [trust_allgmenschen]4
lincom [trust_allgmenschen]3 - [trust_allgmenschen]4

lincom [trust_keinverlass]1 - [trust_keinverlass]2
lincom [trust_keinverlass]1 - [trust_keinverlass]3
lincom [trust_keinverlass]1 - [trust_keinverlass]4
lincom [trust_keinverlass]2 - [trust_keinverlass]3
lincom [trust_keinverlass]2 - [trust_keinverlass]4
lincom [trust_keinverlass]3 - [trust_keinverlass]4

lincom [trust_vorsichtmfremd]1 - [trust_vorsichtmfremd]2
lincom [trust_vorsichtmfremd]1 - [trust_vorsichtmfremd]3
lincom [trust_vorsichtmfremd]1 - [trust_vorsichtmfremd]4
lincom [trust_vorsichtmfremd]2 - [trust_vorsichtmfremd]3
lincom [trust_vorsichtmfremd]2 - [trust_vorsichtmfremd]4
lincom [trust_vorsichtmfremd]3 - [trust_vorsichtmfremd]4

lincom [trust_unternehmen]1 - [trust_unternehmen]2
lincom [trust_unternehmen]1 - [trust_unternehmen]3
lincom [trust_unternehmen]1 - [trust_unternehmen]4
lincom [trust_unternehmen]2 - [trust_unternehmen]3
lincom [trust_unternehmen]2 - [trust_unternehmen]4
lincom [trust_unternehmen]3 - [trust_unternehmen]4

lincom [trust_adsnotrust]1 - [trust_adsnotrust]2
lincom [trust_adsnotrust]1 - [trust_adsnotrust]3
lincom [trust_adsnotrust]1 - [trust_adsnotrust]4
lincom [trust_adsnotrust]2 - [trust_adsnotrust]3
lincom [trust_adsnotrust]2 - [trust_adsnotrust]4
lincom [trust_adsnotrust]3 - [trust_adsnotrust]4

lincom [trust_neueinfoskritisch]1 - [trust_neueinfoskritisch]2
lincom [trust_neueinfoskritisch]1 - [trust_neueinfoskritisch]3
lincom [trust_neueinfoskritisch]1 - [trust_neueinfoskritisch]4
lincom [trust_neueinfoskritisch]2 - [trust_neueinfoskritisch]3
lincom [trust_neueinfoskritisch]2 - [trust_neueinfoskritisch]4
lincom [trust_neueinfoskritisch]3 - [trust_neueinfoskritisch]4


//Norms/Beliefs
eststo: estpost tabstat regfoc_fremdeanl regfoc_regeluvorsch regfoc_verpflicht regfoc_kreativ regfoc_kontrolledislike regfoc_neuartig regfoc_fehlerfrei regfoc_ausprobieren regfoc_anerkennung regfoc_nachdenken psy_spontan psy_kompliziert psy_gegenwartsfok psy_schnellesantw, by (cluster3) stats(mean sd n)
esttab using Descr_norms.rtf, cells ("regfoc_fremdeanl(fmt(2)) regfoc_regeluvorsch(fmt(2)) regfoc_verpflicht(fmt(2)) regfoc_kreativ(fmt(2)) regfoc_kontrolledislike(fmt(2)) regfoc_neuartig(fmt(2)) regfoc_fehlerfrei(fmt(2)) regfoc_ausprobieren(fmt(2)) regfoc_anerkennung(fmt(2)) regfoc_nachdenken(fmt(2)) psy_spontan(fmt(2)) psy_kompliziert(fmt(2)) psy_gegenwartsfok(fmt(2)) psy_schnellesantw(fmt(2))")
eststo clear

mean regfoc_fremdeanl regfoc_regeluvorsch regfoc_verpflicht regfoc_kreativ regfoc_kontrolledislike regfoc_neuartig regfoc_fehlerfrei regfoc_ausprobieren regfoc_anerkennung regfoc_nachdenken psy_spontan psy_kompliziert psy_gegenwartsfok psy_schnellesantw, over(cluster3)
estat sd

lincom [regfoc_fremdeanl]1 - [regfoc_fremdeanl]2
lincom [regfoc_fremdeanl]1 - [regfoc_fremdeanl]3
lincom [regfoc_fremdeanl]1 - [regfoc_fremdeanl]4
lincom [regfoc_fremdeanl]2 - [regfoc_fremdeanl]3
lincom [regfoc_fremdeanl]2 - [regfoc_fremdeanl]4
lincom [regfoc_fremdeanl]3 - [regfoc_fremdeanl]4

lincom [regfoc_regeluvorsch]1 - [regfoc_regeluvorsch]2
lincom [regfoc_regeluvorsch]1 - [regfoc_regeluvorsch]3
lincom [regfoc_regeluvorsch]1 - [regfoc_regeluvorsch]4
lincom [regfoc_regeluvorsch]2 - [regfoc_regeluvorsch]3
lincom [regfoc_regeluvorsch]2 - [regfoc_regeluvorsch]4
lincom [regfoc_regeluvorsch]3 - [regfoc_regeluvorsch]4

lincom [regfoc_verpflicht]1 - [regfoc_verpflicht]2
lincom [regfoc_verpflicht]1 - [regfoc_verpflicht]3
lincom [regfoc_verpflicht]1 - [regfoc_verpflicht]4
lincom [regfoc_verpflicht]2 - [regfoc_verpflicht]3
lincom [regfoc_verpflicht]2 - [regfoc_verpflicht]4
lincom [regfoc_verpflicht]3 - [regfoc_verpflicht]4

lincom [regfoc_kreativ]1 - [regfoc_kreativ]2
lincom [regfoc_kreativ]1 - [regfoc_kreativ]3
lincom [regfoc_kreativ]1 - [regfoc_kreativ]4
lincom [regfoc_kreativ]2 - [regfoc_kreativ]3
lincom [regfoc_kreativ]2 - [regfoc_kreativ]4
lincom [regfoc_kreativ]3 - [regfoc_kreativ]4

lincom [regfoc_kontrolledislike]1 - [regfoc_kontrolledislike]2
lincom [regfoc_kontrolledislike]1 - [regfoc_kontrolledislike]3
lincom [regfoc_kontrolledislike]1 - [regfoc_kontrolledislike]4
lincom [regfoc_kontrolledislike]2 - [regfoc_kontrolledislike]3
lincom [regfoc_kontrolledislike]2 - [regfoc_kontrolledislike]4
lincom [regfoc_kontrolledislike]3 - [regfoc_kontrolledislike]4

lincom [regfoc_neuartig]1 - [regfoc_neuartig]2
lincom [regfoc_neuartig]1 - [regfoc_neuartig]3
lincom [regfoc_neuartig]1 - [regfoc_neuartig]4
lincom [regfoc_neuartig]2 - [regfoc_neuartig]3
lincom [regfoc_neuartig]2 - [regfoc_neuartig]4
lincom [regfoc_neuartig]3 - [regfoc_neuartig]4

lincom [regfoc_fehlerfrei]1 - [regfoc_fehlerfrei]2
lincom [regfoc_fehlerfrei]1 - [regfoc_fehlerfrei]3
lincom [regfoc_fehlerfrei]1 - [regfoc_fehlerfrei]4
lincom [regfoc_fehlerfrei]2 - [regfoc_fehlerfrei]3
lincom [regfoc_fehlerfrei]2 - [regfoc_fehlerfrei]4
lincom [regfoc_fehlerfrei]3 - [regfoc_fehlerfrei]4

lincom [regfoc_ausprobieren]1 - [regfoc_ausprobieren]2
lincom [regfoc_ausprobieren]1 - [regfoc_ausprobieren]3
lincom [regfoc_ausprobieren]1 - [regfoc_ausprobieren]4
lincom [regfoc_ausprobieren]2 - [regfoc_ausprobieren]3
lincom [regfoc_ausprobieren]2 - [regfoc_ausprobieren]4
lincom [regfoc_ausprobieren]3 - [regfoc_ausprobieren]4

lincom [regfoc_anerkennung]1 - [regfoc_anerkennung]2
lincom [regfoc_anerkennung]1 - [regfoc_anerkennung]3
lincom [regfoc_anerkennung]1 - [regfoc_anerkennung]4
lincom [regfoc_anerkennung]2 - [regfoc_anerkennung]3
lincom [regfoc_anerkennung]2 - [regfoc_anerkennung]4
lincom [regfoc_anerkennung]3 - [regfoc_anerkennung]4

lincom [regfoc_nachdenken]1 - [regfoc_nachdenken]2
lincom [regfoc_nachdenken]1 - [regfoc_nachdenken]3
lincom [regfoc_nachdenken]1 - [regfoc_nachdenken]4
lincom [regfoc_nachdenken]2 - [regfoc_nachdenken]3
lincom [regfoc_nachdenken]2 - [regfoc_nachdenken]4
lincom [regfoc_nachdenken]3 - [regfoc_nachdenken]4

lincom [psy_spontan]1 - [psy_spontan]2
lincom [psy_spontan]1 - [psy_spontan]3
lincom [psy_spontan]1 - [psy_spontan]4
lincom [psy_spontan]2 - [psy_spontan]3
lincom [psy_spontan]2 - [psy_spontan]4
lincom [psy_spontan]3 - [psy_spontan]4

lincom [psy_kompliziert]1 - [psy_kompliziert]2
lincom [psy_kompliziert]1 - [psy_kompliziert]3
lincom [psy_kompliziert]1 - [psy_kompliziert]4
lincom [psy_kompliziert]2 - [psy_kompliziert]3
lincom [psy_kompliziert]2 - [psy_kompliziert]4
lincom [psy_kompliziert]3 - [psy_kompliziert]4

lincom [psy_gegenwartsfok]1 - [psy_gegenwartsfok]2
lincom [psy_gegenwartsfok]1 - [psy_gegenwartsfok]3
lincom [psy_gegenwartsfok]1 - [psy_gegenwartsfok]4
lincom [psy_gegenwartsfok]2 - [psy_gegenwartsfok]3
lincom [psy_gegenwartsfok]2 - [psy_gegenwartsfok]4
lincom [psy_gegenwartsfok]3 - [psy_gegenwartsfok]4

lincom [psy_schnellesantw]1 - [psy_schnellesantw]2
lincom [psy_schnellesantw]1 - [psy_schnellesantw]3
lincom [psy_schnellesantw]1 - [psy_schnellesantw]4
lincom [psy_schnellesantw]2 - [psy_schnellesantw]3
lincom [psy_schnellesantw]2 - [psy_schnellesantw]4
lincom [psy_schnellesantw]3 - [psy_schnellesantw]4

//Information channels
eststo: estpost tabstat infsrc_tageszeitung infsrc_zeitschrift infsrc_tv infsrc_radio infsrc_internet infsrc_gespraeche, by (cluster3) stats(mean sd n)
esttab using Descr_info.rtf, cells ("infsrc_tageszeitung(fmt(2)) infsrc_zeitschrift(fmt(2)) infsrc_tv(fmt(2)) infsrc_radio(fmt(2)) infsrc_internet(fmt(2)) infsrc_gespraeche(fmt(2))")
eststo clear

mean infsrc_tageszeitung infsrc_zeitschrift infsrc_tv infsrc_radio infsrc_internet infsrc_gespraeche, over(cluster3)
estat sd

lincom [infsrc_tageszeitung]1 - [infsrc_tageszeitung]2
lincom [infsrc_tageszeitung]1 - [infsrc_tageszeitung]3
lincom [infsrc_tageszeitung]1 - [infsrc_tageszeitung]4
lincom [infsrc_tageszeitung]2 - [infsrc_tageszeitung]3
lincom [infsrc_tageszeitung]2 - [infsrc_tageszeitung]4
lincom [infsrc_tageszeitung]3 - [infsrc_tageszeitung]4

lincom [infsrc_zeitschrift]1 - [infsrc_zeitschrift]2
lincom [infsrc_zeitschrift]1 - [infsrc_zeitschrift]3
lincom [infsrc_zeitschrift]1 - [infsrc_zeitschrift]4
lincom [infsrc_zeitschrift]2 - [infsrc_zeitschrift]3
lincom [infsrc_zeitschrift]2 - [infsrc_zeitschrift]4
lincom [infsrc_zeitschrift]3 - [infsrc_zeitschrift]4

lincom [infsrc_tv]1 - [infsrc_tv]2
lincom [infsrc_tv]1 - [infsrc_tv]3
lincom [infsrc_tv]1 - [infsrc_tv]4
lincom [infsrc_tv]2 - [infsrc_tv]3
lincom [infsrc_tv]2 - [infsrc_tv]4
lincom [infsrc_tv]3 - [infsrc_tv]4

lincom [infsrc_radio]1 - [infsrc_radio]2
lincom [infsrc_radio]1 - [infsrc_radio]3
lincom [infsrc_radio]1 - [infsrc_radio]4
lincom [infsrc_radio]2 - [infsrc_radio]3
lincom [infsrc_radio]2 - [infsrc_radio]4
lincom [infsrc_radio]3 - [infsrc_radio]4

lincom [infsrc_internet]1 - [infsrc_internet]2
lincom [infsrc_internet]1 - [infsrc_internet]3
lincom [infsrc_internet]1 - [infsrc_internet]4
lincom [infsrc_internet]2 - [infsrc_internet]3
lincom [infsrc_internet]2 - [infsrc_internet]4
lincom [infsrc_internet]3 - [infsrc_internet]4

lincom [infsrc_gespraeche]1 - [infsrc_gespraeche]2
lincom [infsrc_gespraeche]1 - [infsrc_gespraeche]3
lincom [infsrc_gespraeche]1 - [infsrc_gespraeche]4
lincom [infsrc_gespraeche]2 - [infsrc_gespraeche]3
lincom [infsrc_gespraeche]2 - [infsrc_gespraeche]4
lincom [infsrc_gespraeche]3 - [infsrc_gespraeche]4


//Topical information channels
//Medizin
eststo: estpost tabstat inf_medizintageszeitung inf_medizinzeitschrift inf_medizintv inf_medizinradio inf_medizininternet inf_medizingespraeche inf_medizingarnicht , by (cluster3) stats(mean sd n)
esttab using Descr_med.rtf, cells ("inf_medizintageszeitung(fmt(2)) inf_medizinzeitschrift(fmt(2)) inf_medizintv(fmt(2)) inf_medizinradio(fmt(2)) inf_medizininternet(fmt(2)) inf_medizingespraeche(fmt(2)) inf_medizingarnicht(fmt(2))")
eststo clear

mean inf_medizintageszeitung inf_medizinzeitschrift inf_medizintv inf_medizinradio inf_medizininternet inf_medizingespraeche inf_medizingarnicht, over(cluster3)
estat sd 

lincom [inf_medizintageszeitung]1 - [inf_medizintageszeitung]2
lincom [inf_medizintageszeitung]1 - [inf_medizintageszeitung]3
lincom [inf_medizintageszeitung]1 - [inf_medizintageszeitung]4
lincom [inf_medizintageszeitung]2 - [inf_medizintageszeitung]3
lincom [inf_medizintageszeitung]2 - [inf_medizintageszeitung]4
lincom [inf_medizintageszeitung]3 - [inf_medizintageszeitung]4

lincom [inf_medizinzeitschrift]1 - [inf_medizinzeitschrift]2
lincom [inf_medizinzeitschrift]1 - [inf_medizinzeitschrift]3
lincom [inf_medizinzeitschrift]1 - [inf_medizinzeitschrift]4
lincom [inf_medizinzeitschrift]2 - [inf_medizinzeitschrift]3
lincom [inf_medizinzeitschrift]2 - [inf_medizinzeitschrift]4
lincom [inf_medizinzeitschrift]3 - [inf_medizinzeitschrift]4

lincom [inf_medizintv]1 - [inf_medizintv]2
lincom [inf_medizintv]1 - [inf_medizintv]3
lincom [inf_medizintv]1 - [inf_medizintv]4
lincom [inf_medizintv]2 - [inf_medizintv]3
lincom [inf_medizintv]2 - [inf_medizintv]4
lincom [inf_medizintv]3 - [inf_medizintv]4

lincom [inf_medizinradio]1 - [inf_medizinradio]2
lincom [inf_medizinradio]1 - [inf_medizinradio]3
lincom [inf_medizinradio]1 - [inf_medizinradio]4
lincom [inf_medizinradio]2 - [inf_medizinradio]3
lincom [inf_medizinradio]2 - [inf_medizinradio]4
lincom [inf_medizinradio]3 - [inf_medizinradio]4

lincom [inf_medizininternet]1 - [inf_medizininternet]2
lincom [inf_medizininternet]1 - [inf_medizininternet]3
lincom [inf_medizininternet]1 - [inf_medizininternet]4
lincom [inf_medizininternet]2 - [inf_medizininternet]3
lincom [inf_medizininternet]2 - [inf_medizininternet]4
lincom [inf_medizininternet]3 - [inf_medizininternet]4

lincom [inf_medizingespraeche]1 - [inf_medizingespraeche]2
lincom [inf_medizingespraeche]1 - [inf_medizingespraeche]3
lincom [inf_medizingespraeche]1 - [inf_medizingespraeche]4
lincom [inf_medizingespraeche]2 - [inf_medizingespraeche]3
lincom [inf_medizingespraeche]2 - [inf_medizingespraeche]4
lincom [inf_medizingespraeche]3 - [inf_medizingespraeche]4

lincom [inf_medizingarnicht]1 - [inf_medizingarnicht]2
lincom [inf_medizingarnicht]1 - [inf_medizingarnicht]3
lincom [inf_medizingarnicht]1 - [inf_medizingarnicht]4
lincom [inf_medizingarnicht]2 - [inf_medizingarnicht]3
lincom [inf_medizingarnicht]2 - [inf_medizingarnicht]4
lincom [inf_medizingarnicht]3 - [inf_medizingarnicht]4


//Technik
eststo: estpost tabstat inf_techniktageszeitung inf_technikzeitschrift inf_techniktv inf_technikradio inf_technikinternet inf_technikgespraeche inf_technikgarnicht , by (cluster3) stats(mean sd n)
esttab using Descr_tech.rtf, cells ("inf_techniktageszeitung(fmt(2)) inf_technikzeitschrift(fmt(2)) inf_techniktv(fmt(2)) inf_technikradio(fmt(2)) inf_technikinternet(fmt(2)) inf_technikgespraeche(fmt(2)) inf_technikgarnicht(fmt(2))")
eststo clear

mean inf_techniktageszeitung inf_technikzeitschrift inf_techniktv inf_technikradio inf_technikinternet inf_technikgespraeche inf_technikgarnicht, over(cluster3)
estat sd

lincom [inf_techniktageszeitung]1 - [inf_techniktageszeitung]2
lincom [inf_techniktageszeitung]1 - [inf_techniktageszeitung]3
lincom [inf_techniktageszeitung]1 - [inf_techniktageszeitung]4
lincom [inf_techniktageszeitung]2 - [inf_techniktageszeitung]3
lincom [inf_techniktageszeitung]2 - [inf_techniktageszeitung]4
lincom [inf_techniktageszeitung]3 - [inf_techniktageszeitung]4

lincom [inf_technikzeitschrift]1 - [inf_technikzeitschrift]2
lincom [inf_technikzeitschrift]1 - [inf_technikzeitschrift]3
lincom [inf_technikzeitschrift]1 - [inf_technikzeitschrift]4
lincom [inf_technikzeitschrift]2 - [inf_technikzeitschrift]3
lincom [inf_technikzeitschrift]2 - [inf_technikzeitschrift]4
lincom [inf_technikzeitschrift]3 - [inf_technikzeitschrift]4

lincom [inf_techniktv]1 - [inf_techniktv]2
lincom [inf_techniktv]1 - [inf_techniktv]3
lincom [inf_techniktv]1 - [inf_techniktv]4
lincom [inf_techniktv]2 - [inf_techniktv]3
lincom [inf_techniktv]2 - [inf_techniktv]4
lincom [inf_techniktv]3 - [inf_techniktv]4

lincom [inf_technikradio]1 - [inf_technikradio]2
lincom [inf_technikradio]1 - [inf_technikradio]3
lincom [inf_technikradio]1 - [inf_technikradio]4
lincom [inf_technikradio]2 - [inf_technikradio]3
lincom [inf_technikradio]2 - [inf_technikradio]4
lincom [inf_technikradio]3 - [inf_technikradio]4

lincom [inf_technikinternet]1 - [inf_technikinternet]2
lincom [inf_technikinternet]1 - [inf_technikinternet]3
lincom [inf_technikinternet]1 - [inf_technikinternet]4
lincom [inf_technikinternet]2 - [inf_technikinternet]3
lincom [inf_technikinternet]2 - [inf_technikinternet]4
lincom [inf_technikinternet]3 - [inf_technikinternet]4

lincom [inf_technikgespraeche]1 - [inf_technikgespraeche]2
lincom [inf_technikgespraeche]1 - [inf_technikgespraeche]3
lincom [inf_technikgespraeche]1 - [inf_technikgespraeche]4
lincom [inf_technikgespraeche]2 - [inf_technikgespraeche]3
lincom [inf_technikgespraeche]2 - [inf_technikgespraeche]4
lincom [inf_technikgespraeche]3 - [inf_technikgespraeche]4

lincom [inf_technikgarnicht]1 - [inf_technikgarnicht]2
lincom [inf_technikgarnicht]1 - [inf_technikgarnicht]3
lincom [inf_technikgarnicht]1 - [inf_technikgarnicht]4
lincom [inf_technikgarnicht]2 - [inf_technikgarnicht]3
lincom [inf_technikgarnicht]2 - [inf_technikgarnicht]4
lincom [inf_technikgarnicht]3 - [inf_technikgarnicht]4


//Sport
eststo: estpost tabstat inf_sporttageszeitung inf_sportzeitschrift inf_sporttv inf_sportradio inf_sportinternet inf_sportgespraeche inf_sportgarnicht, by (cluster3) stats(mean sd n)
esttab using Descr_sport.rtf, cells ("inf_sporttageszeitung(fmt(2)) inf_sportzeitschrift(fmt(2)) inf_sporttv(fmt(2)) inf_sportradio(fmt(2)) inf_sportinternet(fmt(2)) inf_sportgespraeche(fmt(2)) inf_sportgarnicht(fmt(2))")
eststo clear

mean inf_sporttageszeitung inf_sportzeitschrift inf_sporttv inf_sportradio inf_sportinternet inf_sportgespraeche inf_sportgarnicht, over(cluster3)
estat sd

lincom [inf_sporttageszeitung]1 - [inf_sporttageszeitung]2
lincom [inf_sporttageszeitung]1 - [inf_sporttageszeitung]3
lincom [inf_sporttageszeitung]1 - [inf_sporttageszeitung]4
lincom [inf_sporttageszeitung]2 - [inf_sporttageszeitung]3
lincom [inf_sporttageszeitung]2 - [inf_sporttageszeitung]4
lincom [inf_sporttageszeitung]3 - [inf_sporttageszeitung]4

lincom [inf_sportzeitschrift]1 - [inf_sportzeitschrift]2
lincom [inf_sportzeitschrift]1 - [inf_sportzeitschrift]3
lincom [inf_sportzeitschrift]1 - [inf_sportzeitschrift]4
lincom [inf_sportzeitschrift]2 - [inf_sportzeitschrift]3
lincom [inf_sportzeitschrift]2 - [inf_sportzeitschrift]4
lincom [inf_sportzeitschrift]3 - [inf_sportzeitschrift]4

lincom [inf_sporttv]1 - [inf_sporttv]2
lincom [inf_sporttv]1 - [inf_sporttv]3
lincom [inf_sporttv]1 - [inf_sporttv]4
lincom [inf_sporttv]2 - [inf_sporttv]3
lincom [inf_sporttv]2 - [inf_sporttv]4
lincom [inf_sporttv]3 - [inf_sporttv]4

lincom [inf_sportradio]1 - [inf_sportradio]2
lincom [inf_sportradio]1 - [inf_sportradio]3
lincom [inf_sportradio]1 - [inf_sportradio]4
lincom [inf_sportradio]2 - [inf_sportradio]3
lincom [inf_sportradio]2 - [inf_sportradio]4
lincom [inf_sportradio]3 - [inf_sportradio]4

lincom [inf_sportinternet]1 - [inf_sportinternet]2
lincom [inf_sportinternet]1 - [inf_sportinternet]3
lincom [inf_sportinternet]1 - [inf_sportinternet]4
lincom [inf_sportinternet]2 - [inf_sportinternet]3
lincom [inf_sportinternet]2 - [inf_sportinternet]4
lincom [inf_sportinternet]3 - [inf_sportinternet]4

lincom [inf_sportgespraeche]1 - [inf_sportgespraeche]2
lincom [inf_sportgespraeche]1 - [inf_sportgespraeche]3
lincom [inf_sportgespraeche]1 - [inf_sportgespraeche]4
lincom [inf_sportgespraeche]2 - [inf_sportgespraeche]3
lincom [inf_sportgespraeche]2 - [inf_sportgespraeche]4
lincom [inf_sportgespraeche]3 - [inf_sportgespraeche]4

lincom [inf_sportgarnicht]1 - [inf_sportgarnicht]2
lincom [inf_sportgarnicht]1 - [inf_sportgarnicht]3
lincom [inf_sportgarnicht]1 - [inf_sportgarnicht]4
lincom [inf_sportgarnicht]2 - [inf_sportgarnicht]3
lincom [inf_sportgarnicht]2 - [inf_sportgarnicht]4
lincom [inf_sportgarnicht]3 - [inf_sportgarnicht]4


//Kultur
eststo: estpost tabstat inf_kulturtageszeitung inf_kulturzeitschrift inf_kulturtv inf_kulturradio inf_kulturinternet inf_kulturgespraeche inf_kulturgarnicht, by (cluster3) stats(mean sd n)
esttab using Descr_culture.rtf, cells ("inf_kulturtageszeitung(fmt(2)) inf_kulturzeitschrift(fmt(2)) inf_kulturtv(fmt(2)) inf_kulturradio(fmt(2)) inf_kulturinternet(fmt(2)) inf_kulturgespraeche(fmt(2)) inf_kulturgarnicht(fmt(2))")
eststo clear

mean inf_kulturtageszeitung inf_kulturzeitschrift inf_kulturtv inf_kulturradio inf_kulturinternet inf_kulturgespraeche inf_kulturgarnicht, over(cluster3)
estat sd

lincom [inf_kulturtageszeitung]1 - [inf_kulturtageszeitung]2
lincom [inf_kulturtageszeitung]1 - [inf_kulturtageszeitung]3
lincom [inf_kulturtageszeitung]1 - [inf_kulturtageszeitung]4
lincom [inf_kulturtageszeitung]2 - [inf_kulturtageszeitung]3
lincom [inf_kulturtageszeitung]2 - [inf_kulturtageszeitung]4
lincom [inf_kulturtageszeitung]3 - [inf_kulturtageszeitung]4

lincom [inf_kulturzeitschrift]1 - [inf_kulturzeitschrift]2
lincom [inf_kulturzeitschrift]1 - [inf_kulturzeitschrift]3
lincom [inf_kulturzeitschrift]1 - [inf_kulturzeitschrift]4
lincom [inf_kulturzeitschrift]2 - [inf_kulturzeitschrift]3
lincom [inf_kulturzeitschrift]2 - [inf_kulturzeitschrift]4
lincom [inf_kulturzeitschrift]3 - [inf_kulturzeitschrift]4

lincom [inf_kulturtv]1 - [inf_kulturtv]2
lincom [inf_kulturtv]1 - [inf_kulturtv]3
lincom [inf_kulturtv]1 - [inf_kulturtv]4
lincom [inf_kulturtv]2 - [inf_kulturtv]3
lincom [inf_kulturtv]2 - [inf_kulturtv]4
lincom [inf_kulturtv]3 - [inf_kulturtv]4

lincom [inf_kulturradio]1 - [inf_kulturradio]2
lincom [inf_kulturradio]1 - [inf_kulturradio]3
lincom [inf_kulturradio]1 - [inf_kulturradio]4
lincom [inf_kulturradio]2 - [inf_kulturradio]3
lincom [inf_kulturradio]2 - [inf_kulturradio]4
lincom [inf_kulturradio]3 - [inf_kulturradio]4

lincom [inf_kulturinternet]1 - [inf_kulturinternet]2
lincom [inf_kulturinternet]1 - [inf_kulturinternet]3
lincom [inf_kulturinternet]1 - [inf_kulturinternet]4
lincom [inf_kulturinternet]2 - [inf_kulturinternet]3
lincom [inf_kulturinternet]2 - [inf_kulturinternet]4
lincom [inf_kulturinternet]3 - [inf_kulturinternet]4

lincom [inf_kulturgespraeche]1 - [inf_kulturgespraeche]2
lincom [inf_kulturgespraeche]1 - [inf_kulturgespraeche]3
lincom [inf_kulturgespraeche]1 - [inf_kulturgespraeche]4
lincom [inf_kulturgespraeche]2 - [inf_kulturgespraeche]3
lincom [inf_kulturgespraeche]2 - [inf_kulturgespraeche]4
lincom [inf_kulturgespraeche]3 - [inf_kulturgespraeche]4

lincom [inf_kulturgarnicht]1 - [inf_kulturgarnicht]2
lincom [inf_kulturgarnicht]1 - [inf_kulturgarnicht]3
lincom [inf_kulturgarnicht]1 - [inf_kulturgarnicht]4
lincom [inf_kulturgarnicht]2 - [inf_kulturgarnicht]3
lincom [inf_kulturgarnicht]2 - [inf_kulturgarnicht]4
lincom [inf_kulturgarnicht]3 - [inf_kulturgarnicht]4


//Politik
eststo: estpost tabstat inf_politiktageszeitung inf_politikzeitschrift inf_politiktv inf_politikradio inf_politikinternet inf_politikgespraeche inf_politikgarnicht, by (cluster3) stats(mean sd n)
esttab using Descr_pol.rtf, cells ("inf_politiktageszeitung(fmt(2)) inf_politikzeitschrift(fmt(2)) inf_politiktv(fmt(2)) inf_politikradio(fmt(2)) inf_politikinternet(fmt(2)) inf_politikgespraeche(fmt(2)) inf_politikgarnicht(fmt(2))")
eststo clear

mean inf_politiktageszeitung inf_politikzeitschrift inf_politiktv inf_politikradio inf_politikinternet inf_politikgespraeche inf_politikgarnicht, over(cluster3)
estat sd

lincom [inf_politiktageszeitung]1 - [inf_politiktageszeitung]2
lincom [inf_politiktageszeitung]1 - [inf_politiktageszeitung]3
lincom [inf_politiktageszeitung]1 - [inf_politiktageszeitung]4
lincom [inf_politiktageszeitung]2 - [inf_politiktageszeitung]3
lincom [inf_politiktageszeitung]2 - [inf_politiktageszeitung]4
lincom [inf_politiktageszeitung]3 - [inf_politiktageszeitung]4

lincom [inf_politikzeitschrift]1 - [inf_politikzeitschrift]2
lincom [inf_politikzeitschrift]1 - [inf_politikzeitschrift]3
lincom [inf_politikzeitschrift]1 - [inf_politikzeitschrift]4
lincom [inf_politikzeitschrift]2 - [inf_politikzeitschrift]3
lincom [inf_politikzeitschrift]2 - [inf_politikzeitschrift]4
lincom [inf_politikzeitschrift]3 - [inf_politikzeitschrift]4

lincom [inf_politiktv]1 - [inf_politiktv]2
lincom [inf_politiktv]1 - [inf_politiktv]3
lincom [inf_politiktv]1 - [inf_politiktv]4
lincom [inf_politiktv]2 - [inf_politiktv]3
lincom [inf_politiktv]2 - [inf_politiktv]4
lincom [inf_politiktv]3 - [inf_politiktv]4

lincom [inf_politikradio]1 - [inf_politikradio]2
lincom [inf_politikradio]1 - [inf_politikradio]3
lincom [inf_politikradio]1 - [inf_politikradio]4
lincom [inf_politikradio]2 - [inf_politikradio]3
lincom [inf_politikradio]2 - [inf_politikradio]4
lincom [inf_politikradio]3 - [inf_politikradio]4

lincom [inf_politikinternet]1 - [inf_politikinternet]2
lincom [inf_politikinternet]1 - [inf_politikinternet]3
lincom [inf_politikinternet]1 - [inf_politikinternet]4
lincom [inf_politikinternet]2 - [inf_politikinternet]3
lincom [inf_politikinternet]2 - [inf_politikinternet]4
lincom [inf_politikinternet]3 - [inf_politikinternet]4

lincom [inf_politikgespraeche]1 - [inf_politikgespraeche]2
lincom [inf_politikgespraeche]1 - [inf_politikgespraeche]3
lincom [inf_politikgespraeche]1 - [inf_politikgespraeche]4
lincom [inf_politikgespraeche]2 - [inf_politikgespraeche]3
lincom [inf_politikgespraeche]2 - [inf_politikgespraeche]4
lincom [inf_politikgespraeche]3 - [inf_politikgespraeche]4

lincom [inf_politikgarnicht]1 - [inf_politikgarnicht]2
lincom [inf_politikgarnicht]1 - [inf_politikgarnicht]3
lincom [inf_politikgarnicht]1 - [inf_politikgarnicht]4
lincom [inf_politikgarnicht]2 - [inf_politikgarnicht]3
lincom [inf_politikgarnicht]2 - [inf_politikgarnicht]4
lincom [inf_politikgarnicht]3 - [inf_politikgarnicht]4

 
//Umwelt
eststo: estpost tabstat inf_umwelttageszeitung inf_umweltzeitschrift inf_umwelttv inf_umweltradio inf_umweltinternet inf_umweltgespraeche inf_umweltgarnicht, by (cluster3) stats(mean sd n)
esttab using Descr_env.rtf, cells ("inf_umwelttageszeitung(fmt(2)) inf_umweltzeitschrift(fmt(2)) inf_umwelttv(fmt(2)) inf_umweltradio(fmt(2)) inf_umweltinternet(fmt(2)) inf_umweltgespraeche(fmt(2)) inf_umweltgarnicht(fmt(2))")
eststo clear

mean inf_umwelttageszeitung inf_umweltzeitschrift inf_umwelttv inf_umweltradio inf_umweltinternet inf_umweltgespraeche inf_umweltgarnicht, over(cluster3)
estat sd 
 
lincom [inf_umwelttageszeitung]1 - [inf_umwelttageszeitung]2
lincom [inf_umwelttageszeitung]1 - [inf_umwelttageszeitung]3
lincom [inf_umwelttageszeitung]1 - [inf_umwelttageszeitung]4
lincom [inf_umwelttageszeitung]2 - [inf_umwelttageszeitung]3
lincom [inf_umwelttageszeitung]2 - [inf_umwelttageszeitung]4
lincom [inf_umwelttageszeitung]3 - [inf_umwelttageszeitung]4

lincom [inf_umweltzeitschrift]1 - [inf_umweltzeitschrift]2
lincom [inf_umweltzeitschrift]1 - [inf_umweltzeitschrift]3
lincom [inf_umweltzeitschrift]1 - [inf_umweltzeitschrift]4
lincom [inf_umweltzeitschrift]2 - [inf_umweltzeitschrift]3
lincom [inf_umweltzeitschrift]2 - [inf_umweltzeitschrift]4
lincom [inf_umweltzeitschrift]3 - [inf_umweltzeitschrift]4

lincom [inf_umwelttv]1 - [inf_umwelttv]2
lincom [inf_umwelttv]1 - [inf_umwelttv]3
lincom [inf_umwelttv]1 - [inf_umwelttv]4
lincom [inf_umwelttv]2 - [inf_umwelttv]3
lincom [inf_umwelttv]2 - [inf_umwelttv]4
lincom [inf_umwelttv]3 - [inf_umwelttv]4

lincom [inf_umweltradio]1 - [inf_umweltradio]2
lincom [inf_umweltradio]1 - [inf_umweltradio]3
lincom [inf_umweltradio]1 - [inf_umweltradio]4
lincom [inf_umweltradio]2 - [inf_umweltradio]3
lincom [inf_umweltradio]2 - [inf_umweltradio]4
lincom [inf_umweltradio]3 - [inf_umweltradio]4

lincom [inf_umweltinternet]1 - [inf_umweltinternet]2
lincom [inf_umweltinternet]1 - [inf_umweltinternet]3
lincom [inf_umweltinternet]1 - [inf_umweltinternet]4
lincom [inf_umweltinternet]2 - [inf_umweltinternet]3
lincom [inf_umweltinternet]2 - [inf_umweltinternet]4
lincom [inf_umweltinternet]3 - [inf_umweltinternet]4

lincom [inf_umweltgespraeche]1 - [inf_umweltgespraeche]2
lincom [inf_umweltgespraeche]1 - [inf_umweltgespraeche]3
lincom [inf_umweltgespraeche]1 - [inf_umweltgespraeche]4
lincom [inf_umweltgespraeche]2 - [inf_umweltgespraeche]3
lincom [inf_umweltgespraeche]2 - [inf_umweltgespraeche]4
lincom [inf_umweltgespraeche]3 - [inf_umweltgespraeche]4

lincom [inf_umweltgarnicht]1 - [inf_umweltgarnicht]2
lincom [inf_umweltgarnicht]1 - [inf_umweltgarnicht]3
lincom [inf_umweltgarnicht]1 - [inf_umweltgarnicht]4
lincom [inf_umweltgarnicht]2 - [inf_umweltgarnicht]3
lincom [inf_umweltgarnicht]2 - [inf_umweltgarnicht]4
lincom [inf_umweltgarnicht]3 - [inf_umweltgarnicht]4


//Erneuerbare
eststo: estpost tabstat inf_erneurbetageszeitung inf_erneurbezeitschrift inf_erneurbetv inf_erneurberadio inf_erneurbeinternet inf_erneurbegespraeche inf_erneurbegarnicht, by (cluster3) stats(mean sd n)
esttab using Descr_res.rtf, cells ("inf_erneurbetageszeitung(fmt(2)) inf_erneurbezeitschrift(fmt(2)) inf_erneurbetv(fmt(2)) inf_erneurberadio(fmt(2)) inf_erneurbeinternet(fmt(2)) inf_erneurbegespraeche(fmt(2)) inf_erneurbegarnicht(fmt(2))")
eststo clear

mean inf_erneurbetageszeitung inf_erneurbezeitschrift inf_erneurbetv inf_erneurberadio inf_erneurbeinternet inf_erneurbegespraeche inf_erneurbegarnicht, over(cluster3)
estat sd

lincom [inf_erneurbetageszeitung]1 - [inf_erneurbetageszeitung]2
lincom [inf_erneurbetageszeitung]1 - [inf_erneurbetageszeitung]3
lincom [inf_erneurbetageszeitung]1 - [inf_erneurbetageszeitung]4
lincom [inf_erneurbetageszeitung]2 - [inf_erneurbetageszeitung]3
lincom [inf_erneurbetageszeitung]2 - [inf_erneurbetageszeitung]4
lincom [inf_erneurbetageszeitung]3 - [inf_erneurbetageszeitung]4

lincom [inf_erneurbezeitschrift]1 - [inf_erneurbezeitschrift]2
lincom [inf_erneurbezeitschrift]1 - [inf_erneurbezeitschrift]3
lincom [inf_erneurbezeitschrift]1 - [inf_erneurbezeitschrift]4
lincom [inf_erneurbezeitschrift]2 - [inf_erneurbezeitschrift]3
lincom [inf_erneurbezeitschrift]2 - [inf_erneurbezeitschrift]4
lincom [inf_erneurbezeitschrift]3 - [inf_erneurbezeitschrift]4

lincom [inf_erneurbetv]1 - [inf_erneurbetv]2
lincom [inf_erneurbetv]1 - [inf_erneurbetv]3
lincom [inf_erneurbetv]1 - [inf_erneurbetv]4
lincom [inf_erneurbetv]2 - [inf_erneurbetv]3
lincom [inf_erneurbetv]2 - [inf_erneurbetv]4
lincom [inf_erneurbetv]3 - [inf_erneurbetv]4

lincom [inf_erneurberadio]1 - [inf_erneurberadio]2
lincom [inf_erneurberadio]1 - [inf_erneurberadio]3
lincom [inf_erneurberadio]1 - [inf_erneurberadio]4
lincom [inf_erneurberadio]2 - [inf_erneurberadio]3
lincom [inf_erneurberadio]2 - [inf_erneurberadio]4
lincom [inf_erneurberadio]3 - [inf_erneurberadio]4

lincom [inf_erneurbeinternet]1 - [inf_erneurbeinternet]2
lincom [inf_erneurbeinternet]1 - [inf_erneurbeinternet]3
lincom [inf_erneurbeinternet]1 - [inf_erneurbeinternet]4
lincom [inf_erneurbeinternet]2 - [inf_erneurbeinternet]3
lincom [inf_erneurbeinternet]2 - [inf_erneurbeinternet]4
lincom [inf_erneurbeinternet]3 - [inf_erneurbeinternet]4

lincom [inf_erneurbegespraeche]1 - [inf_erneurbegespraeche]2
lincom [inf_erneurbegespraeche]1 - [inf_erneurbegespraeche]3
lincom [inf_erneurbegespraeche]1 - [inf_erneurbegespraeche]4
lincom [inf_erneurbegespraeche]2 - [inf_erneurbegespraeche]3
lincom [inf_erneurbegespraeche]2 - [inf_erneurbegespraeche]4
lincom [inf_erneurbegespraeche]3 - [inf_erneurbegespraeche]4

lincom [inf_erneurbegarnicht]1 - [inf_erneurbegarnicht]2
lincom [inf_erneurbegarnicht]1 - [inf_erneurbegarnicht]3
lincom [inf_erneurbegarnicht]1 - [inf_erneurbegarnicht]4
lincom [inf_erneurbegarnicht]2 - [inf_erneurbegarnicht]3
lincom [inf_erneurbegarnicht]2 - [inf_erneurbegarnicht]4
lincom [inf_erneurbegarnicht]3 - [inf_erneurbegarnicht]4


//Stromspeicher
eststo: estpost tabstat inf_speichertageszeitung inf_speicherzeitschrift inf_strspeichertv inf_strspeicherradio inf_strspeicherinternet inf_strspeichergespraeche inf_strspeichergarnicht, by (cluster3) stats(mean sd n)
esttab using Descr_storage.rtf, cells ("inf_speichertageszeitung(fmt(2)) inf_speicherzeitschrift(fmt(2)) inf_strspeichertv(fmt(2)) inf_strspeicherradio(fmt(2)) inf_strspeicherinternet(fmt(2)) inf_strspeichergespraeche(fmt(2)) inf_strspeichergarnicht(fmt(2))")
eststo clear

mean inf_speichertageszeitung inf_speicherzeitschrift inf_strspeichertv inf_strspeicherradio inf_strspeicherinternet inf_strspeichergespraeche inf_strspeichergarnicht, over(cluster3)
estat sd

lincom [inf_speichertageszeitung]1 - [inf_speichertageszeitung]2
lincom [inf_speichertageszeitung]1 - [inf_speichertageszeitung]3
lincom [inf_speichertageszeitung]1 - [inf_speichertageszeitung]4
lincom [inf_speichertageszeitung]2 - [inf_speichertageszeitung]3
lincom [inf_speichertageszeitung]2 - [inf_speichertageszeitung]4
lincom [inf_speichertageszeitung]3 - [inf_speichertageszeitung]4

lincom [inf_speicherzeitschrift]1 - [inf_speicherzeitschrift]2
lincom [inf_speicherzeitschrift]1 - [inf_speicherzeitschrift]3
lincom [inf_speicherzeitschrift]1 - [inf_speicherzeitschrift]4
lincom [inf_speicherzeitschrift]2 - [inf_speicherzeitschrift]3
lincom [inf_speicherzeitschrift]2 - [inf_speicherzeitschrift]4
lincom [inf_speicherzeitschrift]3 - [inf_speicherzeitschrift]4

lincom [inf_strspeichertv]1 - [inf_strspeichertv]2
lincom [inf_strspeichertv]1 - [inf_strspeichertv]3
lincom [inf_strspeichertv]1 - [inf_strspeichertv]4
lincom [inf_strspeichertv]2 - [inf_strspeichertv]3
lincom [inf_strspeichertv]2 - [inf_strspeichertv]4
lincom [inf_strspeichertv]3 - [inf_strspeichertv]4

lincom [inf_strspeicherradio]1 - [inf_strspeicherradio]2
lincom [inf_strspeicherradio]1 - [inf_strspeicherradio]3
lincom [inf_strspeicherradio]1 - [inf_strspeicherradio]4
lincom [inf_strspeicherradio]2 - [inf_strspeicherradio]3
lincom [inf_strspeicherradio]2 - [inf_strspeicherradio]4
lincom [inf_strspeicherradio]3 - [inf_strspeicherradio]4

lincom [inf_strspeicherinternet]1 - [inf_strspeicherinternet]2
lincom [inf_strspeicherinternet]1 - [inf_strspeicherinternet]3
lincom [inf_strspeicherinternet]1 - [inf_strspeicherinternet]4
lincom [inf_strspeicherinternet]2 - [inf_strspeicherinternet]3
lincom [inf_strspeicherinternet]2 - [inf_strspeicherinternet]4
lincom [inf_strspeicherinternet]3 - [inf_strspeicherinternet]4

lincom [inf_strspeichergespraeche]1 - [inf_strspeichergespraeche]2
lincom [inf_strspeichergespraeche]1 - [inf_strspeichergespraeche]3
lincom [inf_strspeichergespraeche]1 - [inf_strspeichergespraeche]4
lincom [inf_strspeichergespraeche]2 - [inf_strspeichergespraeche]3
lincom [inf_strspeichergespraeche]2 - [inf_strspeichergespraeche]4
lincom [inf_strspeichergespraeche]3 - [inf_strspeichergespraeche]4

lincom [inf_strspeichergarnicht]1 - [inf_strspeichergarnicht]2
lincom [inf_strspeichergarnicht]1 - [inf_strspeichergarnicht]3
lincom [inf_strspeichergarnicht]1 - [inf_strspeichergarnicht]4
lincom [inf_strspeichergarnicht]2 - [inf_strspeichergarnicht]3
lincom [inf_strspeichergarnicht]2 - [inf_strspeichergarnicht]4
lincom [inf_strspeichergarnicht]3 - [inf_strspeichergarnicht]4



//Knowledge, Interests
eststo: estpost tabstat wissen_medizin wissen_technik wissen_sport wissen_kultur wissen_politik wissen_umwelt wissen_erneuerben wissen_strspeicher, by (cluster3) stats(mean sd n)
esttab using Descr_knowledge.rtf, cells ("wissen_medizin(fmt(2)) wissen_technik(fmt(2)) wissen_sport(fmt(2)) wissen_kultur(fmt(2)) wissen_politik(fmt(2)) wissen_umwelt(fmt(2)) wissen_erneuerben(fmt(2)) wissen_strspeicher(fmt(2))")
eststo clear

mean wissen_medizin wissen_technik wissen_sport wissen_kultur wissen_politik wissen_umwelt wissen_erneuerben wissen_strspeicher, over(cluster3)
estat sd

lincom [wissen_medizin]1 - [wissen_medizin]2
lincom [wissen_medizin]1 - [wissen_medizin]3
lincom [wissen_medizin]1 - [wissen_medizin]4
lincom [wissen_medizin]2 - [wissen_medizin]3
lincom [wissen_medizin]2 - [wissen_medizin]4
lincom [wissen_medizin]3 - [wissen_medizin]4

lincom [wissen_technik]1 - [wissen_technik]2
lincom [wissen_technik]1 - [wissen_technik]3
lincom [wissen_technik]1 - [wissen_technik]4
lincom [wissen_technik]2 - [wissen_technik]3
lincom [wissen_technik]2 - [wissen_technik]4
lincom [wissen_technik]3 - [wissen_technik]4

lincom [wissen_sport]1 - [wissen_sport]2
lincom [wissen_sport]1 - [wissen_sport]3
lincom [wissen_sport]1 - [wissen_sport]4
lincom [wissen_sport]2 - [wissen_sport]3
lincom [wissen_sport]2 - [wissen_sport]4
lincom [wissen_sport]3 - [wissen_sport]4

lincom [wissen_kultur]1 - [wissen_kultur]2
lincom [wissen_kultur]1 - [wissen_kultur]3
lincom [wissen_kultur]1 - [wissen_kultur]4
lincom [wissen_kultur]2 - [wissen_kultur]3
lincom [wissen_kultur]2 - [wissen_kultur]4
lincom [wissen_kultur]3 - [wissen_kultur]4

lincom [wissen_politik]1 - [wissen_politik]2
lincom [wissen_politik]1 - [wissen_politik]3
lincom [wissen_politik]1 - [wissen_politik]4
lincom [wissen_politik]2 - [wissen_politik]3
lincom [wissen_politik]2 - [wissen_politik]4
lincom [wissen_politik]3 - [wissen_politik]4

lincom [wissen_umwelt]1 - [wissen_umwelt]2
lincom [wissen_umwelt]1 - [wissen_umwelt]3
lincom [wissen_umwelt]1 - [wissen_umwelt]4
lincom [wissen_umwelt]2 - [wissen_umwelt]3
lincom [wissen_umwelt]2 - [wissen_umwelt]4
lincom [wissen_umwelt]3 - [wissen_umwelt]4

lincom [wissen_erneuerben]1 - [wissen_erneuerben]2
lincom [wissen_erneuerben]1 - [wissen_erneuerben]3
lincom [wissen_erneuerben]1 - [wissen_erneuerben]4
lincom [wissen_erneuerben]2 - [wissen_erneuerben]3
lincom [wissen_erneuerben]2 - [wissen_erneuerben]4
lincom [wissen_erneuerben]3 - [wissen_erneuerben]4

lincom [wissen_strspeicher]1 - [wissen_strspeicher]2
lincom [wissen_strspeicher]1 - [wissen_strspeicher]3
lincom [wissen_strspeicher]1 - [wissen_strspeicher]4
lincom [wissen_strspeicher]2 - [wissen_strspeicher]3
lincom [wissen_strspeicher]2 - [wissen_strspeicher]4
lincom [wissen_strspeicher]3 - [wissen_strspeicher]4


//Use of media, technology perception
eststo: estpost tabstat use_computer use_internet use_handy einflusszuk_pv einflusszuk_atome einflusszuk_winden einflusszuk_strspeicher einflusszuk_it einflusszuk_biotech einflusszuk_nanotech, by (cluster3) stats(mean sd n)
esttab using Descr_media.rtf, cells ("use_computer(fmt(2)) use_internet(fmt(2)) use_handy(fmt(2)) einflusszuk_pv(fmt(2)) einflusszuk_atome(fmt(2)) einflusszuk_winden(fmt(2)) einflusszuk_strspeicher(fmt(2)) einflusszuk_it(fmt(2)) einflusszuk_biotech(fmt(2)) einflusszuk_nanotech(fmt(2))")
eststo clear

mean use_computer use_internet use_handy einflusszuk_pv einflusszuk_atome einflusszuk_winden einflusszuk_strspeicher einflusszuk_it einflusszuk_biotech einflusszuk_nanotech, over(cluster3)
estat sd

lincom [use_computer]1 - [use_computer]2
lincom [use_computer]1 - [use_computer]3
lincom [use_computer]1 - [use_computer]4
lincom [use_computer]2 - [use_computer]3
lincom [use_computer]2 - [use_computer]4
lincom [use_computer]3 - [use_computer]4

lincom [use_internet]1 - [use_internet]2
lincom [use_internet]1 - [use_internet]3
lincom [use_internet]1 - [use_internet]4
lincom [use_internet]2 - [use_internet]3
lincom [use_internet]2 - [use_internet]4
lincom [use_internet]3 - [use_internet]4

lincom [use_handy]1 - [use_handy]2
lincom [use_handy]1 - [use_handy]3
lincom [use_handy]1 - [use_handy]4
lincom [use_handy]2 - [use_handy]3
lincom [use_handy]2 - [use_handy]4
lincom [use_handy]3 - [use_handy]4

mean einflusszuk_pv if einflusszuk_pv<7, over(cluster3)
estat sd
lincom [einflusszuk_pv]1 - [einflusszuk_pv]2
lincom [einflusszuk_pv]1 - [einflusszuk_pv]3
lincom [einflusszuk_pv]1 - [einflusszuk_pv]4
lincom [einflusszuk_pv]2 - [einflusszuk_pv]3
lincom [einflusszuk_pv]2 - [einflusszuk_pv]4
lincom [einflusszuk_pv]3 - [einflusszuk_pv]4
tab einflusszuk_pv cluster3 if einflusszuk_pv<7
mean einflusszuk_pv if einflusszuk_pv<7
estat sd

mean einflusszuk_atome if einflusszuk_atome<7, over(cluster3)
estat sd
lincom [einflusszuk_atome]1 - [einflusszuk_atome]2
lincom [einflusszuk_atome]1 - [einflusszuk_atome]3
lincom [einflusszuk_atome]1 - [einflusszuk_atome]4
lincom [einflusszuk_atome]2 - [einflusszuk_atome]3
lincom [einflusszuk_atome]2 - [einflusszuk_atome]4
lincom [einflusszuk_atome]3 - [einflusszuk_atome]4
tab einflusszuk_atome cluster3 if einflusszuk_atome<7
mean einflusszuk_atome if einflusszuk_atome<7
estat sd

mean einflusszuk_winden if einflusszuk_winden<7, over(cluster3)
estat sd
lincom [einflusszuk_winden]1 - [einflusszuk_winden]2
lincom [einflusszuk_winden]1 - [einflusszuk_winden]3
lincom [einflusszuk_winden]1 - [einflusszuk_winden]4
lincom [einflusszuk_winden]2 - [einflusszuk_winden]3
lincom [einflusszuk_winden]2 - [einflusszuk_winden]4
lincom [einflusszuk_winden]3 - [einflusszuk_winden]4
tab einflusszuk_winden cluster3 if einflusszuk_winden<7
mean einflusszuk_winden if einflusszuk_winden<7
estat sd

mean einflusszuk_strspeicher if einflusszuk_strspeicher<7, over(cluster3)
estat sd
lincom [einflusszuk_strspeicher]1 - [einflusszuk_strspeicher]2
lincom [einflusszuk_strspeicher]1 - [einflusszuk_strspeicher]3
lincom [einflusszuk_strspeicher]1 - [einflusszuk_strspeicher]4
lincom [einflusszuk_strspeicher]2 - [einflusszuk_strspeicher]3
lincom [einflusszuk_strspeicher]2 - [einflusszuk_strspeicher]4
lincom [einflusszuk_strspeicher]3 - [einflusszuk_strspeicher]4
tab einflusszuk_strspeicher cluster3 if einflusszuk_strspeicher<7
mean einflusszuk_strspeicher if einflusszuk_strspeicher<7
estat sd

mean einflusszuk_it if einflusszuk_it<7, over(cluster3)
estat sd
lincom [einflusszuk_it]1 - [einflusszuk_it]2
lincom [einflusszuk_it]1 - [einflusszuk_it]3
lincom [einflusszuk_it]1 - [einflusszuk_it]4
lincom [einflusszuk_it]2 - [einflusszuk_it]3
lincom [einflusszuk_it]2 - [einflusszuk_it]4
lincom [einflusszuk_it]3 - [einflusszuk_it]4
tab einflusszuk_it cluster3 if einflusszuk_it<7
mean einflusszuk_it if einflusszuk_it<7
estat sd

mean einflusszuk_biotech if einflusszuk_biotech<7, over(cluster3)
estat sd
lincom [einflusszuk_biotech]1 - [einflusszuk_biotech]2
lincom [einflusszuk_biotech]1 - [einflusszuk_biotech]3
lincom [einflusszuk_biotech]1 - [einflusszuk_biotech]4
lincom [einflusszuk_biotech]2 - [einflusszuk_biotech]3
lincom [einflusszuk_biotech]2 - [einflusszuk_biotech]4
lincom [einflusszuk_biotech]3 - [einflusszuk_biotech]4
tab einflusszuk_biotech cluster3 if einflusszuk_biotech<7
mean einflusszuk_biotech if einflusszuk_biotech<7
estat sd

mean einflusszuk_nanotech if einflusszuk_nanotech<7, over(cluster3)
estat sd
lincom [einflusszuk_nanotech]1 - [einflusszuk_nanotech]2
lincom [einflusszuk_nanotech]1 - [einflusszuk_nanotech]3
lincom [einflusszuk_nanotech]1 - [einflusszuk_nanotech]4
lincom [einflusszuk_nanotech]2 - [einflusszuk_nanotech]3
lincom [einflusszuk_nanotech]2 - [einflusszuk_nanotech]4
lincom [einflusszuk_nanotech]3 - [einflusszuk_nanotech]4
tab einflusszuk_nanotech cluster3 if einflusszuk_nanotech<7
mean einflusszuk_nanotech if einflusszuk_nanotech<7
estat sd

//Environmental awareness
eststo: estpost tabstat env_spende env_demo env_inforveranst umweltschutz_wichtig auswirk_umweltprobl fwende_persoenlich fwende_deutschl fwende_welt fwende_kommendgen nutzung_oekostr nutzung_effheiz nutzung_waermedae nutzung_fenstergl nutzung_verbrauchsarmeger pv_fuer_eigenverbrauch inbetriebnahme kw, by (cluster3) stats(mean sd n)
esttab using Descr_envawareness.rtf, cells ("env_spende(fmt(2)) env_demo(fmt(2)) env_inforveranst(fmt(2)) umweltschutz_wichtig(fmt(2)) auswirk_umweltprobl(fmt(2)) fwende_persoenlich(fmt(2)) fwende_deutschl(fmt(2)) fwende_welt(fmt(2)) fwende_kommendgen(fmt(2)) nutzung_oekostr(fmt(2)) nutzung_effheiz(fmt(2)) nutzung_waermedae(fmt(2)) nutzung_fenstergl(fmt(2)) nutzung_verbrauchsarmeger(fmt(2)) pv_fuer_eigenverbrauch(fmt(2)) inbetriebnahme(fmt(2)) kw(fmt(2))")
eststo clear

mean env_spende env_demo env_inforveranst umweltschutz_wichtig auswirk_umweltprobl fwende_persoenlich fwende_deutschl fwende_welt fwende_kommendgen nutzung_oekostr nutzung_effheiz nutzung_waermedae nutzung_fenstergl nutzung_verbrauchsarmeger pv_fuer_eigenverbrauch inbetriebnahme kw, over(cluster3)
estat sd

lincom [env_spende]1 - [env_spende]2
lincom [env_spende]1 - [env_spende]3
lincom [env_spende]1 - [env_spende]4
lincom [env_spende]2 - [env_spende]3
lincom [env_spende]2 - [env_spende]4
lincom [env_spende]3 - [env_spende]4

lincom [env_demo]1 - [env_demo]2
lincom [env_demo]1 - [env_demo]3
lincom [env_demo]1 - [env_demo]4
lincom [env_demo]2 - [env_demo]3
lincom [env_demo]2 - [env_demo]4
lincom [env_demo]3 - [env_demo]4

lincom [env_inforveranst]1 - [env_inforveranst]2
lincom [env_inforveranst]1 - [env_inforveranst]3
lincom [env_inforveranst]1 - [env_inforveranst]4
lincom [env_inforveranst]2 - [env_inforveranst]3
lincom [env_inforveranst]2 - [env_inforveranst]4
lincom [env_inforveranst]3 - [env_inforveranst]4

lincom [umweltschutz_wichtig]1 - [umweltschutz_wichtig]2
lincom [umweltschutz_wichtig]1 - [umweltschutz_wichtig]3
lincom [umweltschutz_wichtig]1 - [umweltschutz_wichtig]4
lincom [umweltschutz_wichtig]2 - [umweltschutz_wichtig]3
lincom [umweltschutz_wichtig]2 - [umweltschutz_wichtig]4
lincom [umweltschutz_wichtig]3 - [umweltschutz_wichtig]4

lincom [auswirk_umweltprobl]1 - [auswirk_umweltprobl]2
lincom [auswirk_umweltprobl]1 - [auswirk_umweltprobl]3
lincom [auswirk_umweltprobl]1 - [auswirk_umweltprobl]4
lincom [auswirk_umweltprobl]2 - [auswirk_umweltprobl]3
lincom [auswirk_umweltprobl]2 - [auswirk_umweltprobl]4
lincom [auswirk_umweltprobl]3 - [auswirk_umweltprobl]4

mean fwende_persoenlich if fwende_persoenlich<7, over(cluster3)
estat sd
lincom [fwende_persoenlich]1 - [fwende_persoenlich]2
lincom [fwende_persoenlich]1 - [fwende_persoenlich]3
lincom [fwende_persoenlich]1 - [fwende_persoenlich]4
lincom [fwende_persoenlich]2 - [fwende_persoenlich]3
lincom [fwende_persoenlich]2 - [fwende_persoenlich]4
lincom [fwende_persoenlich]3 - [fwende_persoenlich]4
tab fwende_persoenlich cluster3 if fwende_persoenlich<7
mean fwende_persoenlich if fwende_persoenlich<7
estat sd

mean fwende_deutschl if fwende_deutschl<7, over(cluster3)
estat sd
lincom [fwende_deutschl]1 - [fwende_deutschl]2
lincom [fwende_deutschl]1 - [fwende_deutschl]3
lincom [fwende_deutschl]1 - [fwende_deutschl]4
lincom [fwende_deutschl]2 - [fwende_deutschl]3
lincom [fwende_deutschl]2 - [fwende_deutschl]4
lincom [fwende_deutschl]3 - [fwende_deutschl]4
tab fwende_deutschl cluster3 if fwende_deutschl<7
mean fwende_deutschl if fwende_deutschl<7
estat sd

mean fwende_welt if fwende_welt<7, over(cluster3)
estat sd
lincom [fwende_welt]1 - [fwende_welt]2
lincom [fwende_welt]1 - [fwende_welt]3
lincom [fwende_welt]1 - [fwende_welt]4
lincom [fwende_welt]2 - [fwende_welt]3
lincom [fwende_welt]2 - [fwende_welt]4
lincom [fwende_welt]3 - [fwende_welt]4
tab fwende_welt cluster3 if fwende_welt<7
mean fwende_welt if fwende_welt<7
estat sd

mean fwende_kommendgen if fwende_kommendgen<7, over(cluster3)
estat sd
lincom [fwende_kommendgen]1 - [fwende_kommendgen]2
lincom [fwende_kommendgen]1 - [fwende_kommendgen]3
lincom [fwende_kommendgen]1 - [fwende_kommendgen]4
lincom [fwende_kommendgen]2 - [fwende_kommendgen]3
lincom [fwende_kommendgen]2 - [fwende_kommendgen]4
lincom [fwende_kommendgen]3 - [fwende_kommendgen]4
tab fwende_kommendgen cluster3 if fwende_kommendgen<7
mean fwende_kommendgen if fwende_kommendgen<7
estat sd

lincom [nutzung_oekostr]1 - [nutzung_oekostr]2
lincom [nutzung_oekostr]1 - [nutzung_oekostr]3
lincom [nutzung_oekostr]1 - [nutzung_oekostr]4
lincom [nutzung_oekostr]2 - [nutzung_oekostr]3
lincom [nutzung_oekostr]2 - [nutzung_oekostr]4
lincom [nutzung_oekostr]3 - [nutzung_oekostr]4

lincom [nutzung_effheiz]1 - [nutzung_effheiz]2
lincom [nutzung_effheiz]1 - [nutzung_effheiz]3
lincom [nutzung_effheiz]1 - [nutzung_effheiz]4
lincom [nutzung_effheiz]2 - [nutzung_effheiz]3
lincom [nutzung_effheiz]2 - [nutzung_effheiz]4
lincom [nutzung_effheiz]3 - [nutzung_effheiz]4

lincom [nutzung_waermedae]1 - [nutzung_waermedae]2
lincom [nutzung_waermedae]1 - [nutzung_waermedae]3
lincom [nutzung_waermedae]1 - [nutzung_waermedae]4
lincom [nutzung_waermedae]2 - [nutzung_waermedae]3
lincom [nutzung_waermedae]2 - [nutzung_waermedae]4
lincom [nutzung_waermedae]3 - [nutzung_waermedae]4

lincom [nutzung_fenstergl]1 - [nutzung_fenstergl]2
lincom [nutzung_fenstergl]1 - [nutzung_fenstergl]3
lincom [nutzung_fenstergl]1 - [nutzung_fenstergl]4
lincom [nutzung_fenstergl]2 - [nutzung_fenstergl]3
lincom [nutzung_fenstergl]2 - [nutzung_fenstergl]4
lincom [nutzung_fenstergl]3 - [nutzung_fenstergl]4

lincom [fwende_deutschl]1 - [fwende_deutschl]2
lincom [fwende_deutschl]1 - [fwende_deutschl]3
lincom [fwende_deutschl]1 - [fwende_deutschl]4
lincom [fwende_deutschl]2 - [fwende_deutschl]3
lincom [fwende_deutschl]2 - [fwende_deutschl]4
lincom [fwende_deutschl]3 - [fwende_deutschl]4

lincom [nutzung_verbrauchsarmeger]1 - [nutzung_verbrauchsarmeger]2
lincom [nutzung_verbrauchsarmeger]1 - [nutzung_verbrauchsarmeger]3
lincom [nutzung_verbrauchsarmeger]1 - [nutzung_verbrauchsarmeger]4
lincom [nutzung_verbrauchsarmeger]2 - [nutzung_verbrauchsarmeger]3
lincom [nutzung_verbrauchsarmeger]2 - [nutzung_verbrauchsarmeger]4
lincom [nutzung_verbrauchsarmeger]3 - [nutzung_verbrauchsarmeger]4

lincom [pv_fuer_eigenverbrauch]1 - [pv_fuer_eigenverbrauch]2
lincom [pv_fuer_eigenverbrauch]1 - [pv_fuer_eigenverbrauch]3
lincom [pv_fuer_eigenverbrauch]1 - [pv_fuer_eigenverbrauch]4
lincom [pv_fuer_eigenverbrauch]2 - [pv_fuer_eigenverbrauch]3
lincom [pv_fuer_eigenverbrauch]2 - [pv_fuer_eigenverbrauch]4
lincom [pv_fuer_eigenverbrauch]3 - [pv_fuer_eigenverbrauch]4

lincom [inbetriebnahme]1 - [inbetriebnahme]2
lincom [inbetriebnahme]1 - [inbetriebnahme]3
lincom [inbetriebnahme]1 - [inbetriebnahme]4
lincom [inbetriebnahme]2 - [inbetriebnahme]3
lincom [inbetriebnahme]2 - [inbetriebnahme]4
lincom [inbetriebnahme]3 - [inbetriebnahme]4

lincom [kw]1 - [kw]2
lincom [kw]1 - [kw]3
lincom [kw]1 - [kw]4
lincom [kw]2 - [kw]3
lincom [kw]2 - [kw]4
lincom [kw]3 - [kw]4

//Energy storage knowledge and research
eststo: estpost tabstat prev_knowstore prev_talkstore prev_researchstore post_talkstore post_researchstore wer_sucht_infos wer_entscheidet, by (cluster3) stats(mean sd n)
esttab using Descr_storage.rtf, cells ("prev_knowstore(fmt(2)) prev_talkstore(fmt(2)) prev_researchstore(fmt(2)) post_talkstore(fmt(2)) post_researchstore(fmt(2)) wer_sucht_infos(fmt(2)) wer_entscheidet(fmt(2))")
eststo clear

mean prev_knowstore prev_talkstore prev_researchstore post_talkstore post_researchstore wer_sucht_infos wer_entscheidet, over(cluster3)
estat sd

lincom [prev_knowstore]1 - [prev_knowstore]2
lincom [prev_knowstore]1 - [prev_knowstore]3
lincom [prev_knowstore]1 - [prev_knowstore]4
lincom [prev_knowstore]2 - [prev_knowstore]3
lincom [prev_knowstore]2 - [prev_knowstore]4
lincom [prev_knowstore]3 - [prev_knowstore]4

lincom [prev_talkstore]1 - [prev_talkstore]2
lincom [prev_talkstore]1 - [prev_talkstore]3
lincom [prev_talkstore]1 - [prev_talkstore]4
lincom [prev_talkstore]2 - [prev_talkstore]3
lincom [prev_talkstore]2 - [prev_talkstore]4
lincom [prev_talkstore]3 - [prev_talkstore]4

lincom [prev_researchstore]1 - [prev_researchstore]2
lincom [prev_researchstore]1 - [prev_researchstore]3
lincom [prev_researchstore]1 - [prev_researchstore]4
lincom [prev_researchstore]2 - [prev_researchstore]3
lincom [prev_researchstore]2 - [prev_researchstore]4
lincom [prev_researchstore]3 - [prev_researchstore]4

lincom [post_talkstore]1 - [post_talkstore]2
lincom [post_talkstore]1 - [post_talkstore]3
lincom [post_talkstore]1 - [post_talkstore]4
lincom [post_talkstore]2 - [post_talkstore]3
lincom [post_talkstore]2 - [post_talkstore]4
lincom [post_talkstore]3 - [post_talkstore]4

lincom [post_researchstore]1 - [post_researchstore]2
lincom [post_researchstore]1 - [post_researchstore]3
lincom [post_researchstore]1 - [post_researchstore]4
lincom [post_researchstore]2 - [post_researchstore]3
lincom [post_researchstore]2 - [post_researchstore]4
lincom [post_researchstore]3 - [post_researchstore]4

lincom [wer_sucht_infos]1 - [wer_sucht_infos]2
lincom [wer_sucht_infos]1 - [wer_sucht_infos]3
lincom [wer_sucht_infos]1 - [wer_sucht_infos]4
lincom [wer_sucht_infos]2 - [wer_sucht_infos]3
lincom [wer_sucht_infos]2 - [wer_sucht_infos]4
lincom [wer_sucht_infos]3 - [wer_sucht_infos]4

lincom [wer_entscheidet]1 - [wer_entscheidet]2
lincom [wer_entscheidet]1 - [wer_entscheidet]3
lincom [wer_entscheidet]1 - [wer_entscheidet]4
lincom [wer_entscheidet]2 - [wer_entscheidet]3
lincom [wer_entscheidet]2 - [wer_entscheidet]4
lincom [wer_entscheidet]3 - [wer_entscheidet]4

//Roles
eststo: estpost tabstat researchno researchboth researchpartner researchme decisionno decisionboth decisionpartner decisionme, by (cluster3) stats(mean sd n)
esttab using Descr_Roles.rtf, cells ("researchno(fmt(2)) researchboth(fmt(2)) researchpartner(fmt(2)) researchme(fmt(2)) decisionno(fmt(2)) decisionboth(fmt(2)) decisionpartner(fmt(2)) decisionme(fmt(2))")
eststo clear

mean researchno researchboth researchpartner researchme decisionno decisionboth decisionpartner decisionme, over(cluster3)
estat sd

lincom [researchno]1 - [researchno]2
lincom [researchno]1 - [researchno]3
lincom [researchno]1 - [researchno]4
lincom [researchno]2 - [researchno]3
lincom [researchno]2 - [researchno]4
lincom [researchno]3 - [researchno]4

lincom [researchboth]1 - [researchboth]2
lincom [researchboth]1 - [researchboth]3
lincom [researchboth]1 - [researchboth]4
lincom [researchboth]2 - [researchboth]3
lincom [researchboth]2 - [researchboth]4
lincom [researchboth]3 - [researchboth]4

lincom [researchpartner]1 - [researchpartner]2
lincom [researchpartner]1 - [researchpartner]3
lincom [researchpartner]1 - [researchpartner]4
lincom [researchpartner]2 - [researchpartner]3
lincom [researchpartner]2 - [researchpartner]4
lincom [researchpartner]3 - [researchpartner]4

lincom [researchme]1 - [researchme]2
lincom [researchme]1 - [researchme]3
lincom [researchme]1 - [researchme]4
lincom [researchme]2 - [researchme]3
lincom [researchme]2 - [researchme]4
lincom [researchme]3 - [researchme]4

lincom [decisionno]1 - [decisionno]2
lincom [decisionno]1 - [decisionno]3
lincom [decisionno]1 - [decisionno]4
lincom [decisionno]2 - [decisionno]3
lincom [decisionno]2 - [decisionno]4
lincom [decisionno]3 - [decisionno]4

lincom [decisionboth]1 - [decisionboth]2
lincom [decisionboth]1 - [decisionboth]3
lincom [decisionboth]1 - [decisionboth]4
lincom [decisionboth]2 - [decisionboth]3
lincom [decisionboth]2 - [decisionboth]4
lincom [decisionboth]3 - [decisionboth]4

lincom [decisionpartner]1 - [decisionpartner]2
lincom [decisionpartner]1 - [decisionpartner]3
lincom [decisionpartner]1 - [decisionpartner]4
lincom [decisionpartner]2 - [decisionpartner]3
lincom [decisionpartner]2 - [decisionpartner]4
lincom [decisionpartner]3 - [decisionpartner]4

lincom [decisionme]1 - [decisionme]2
lincom [decisionme]1 - [decisionme]3
lincom [decisionme]1 - [decisionme]4
lincom [decisionme]2 - [decisionme]3
lincom [decisionme]2 - [decisionme]4
lincom [decisionme]3 - [decisionme]4

//Purchase criteria
eststo: estpost tabstat bed_beitrwende bed_innovtechn bed_finasp bed_verssich bed_beitrnetzst bed_infobera invest invest_coorientation, by (cluster3) stats(mean sd n)
esttab using Descr_Purchase.rtf, cells ("bed_beitrwende(fmt(2)) bed_innovtechn(fmt(2)) bed_finasp(fmt(2)) bed_verssich(fmt(2)) bed_beitrnetzst(fmt(2)) bed_infobera(fmt(2)) invest(fmt(2)) invest_coorientation(fmt(2))")
eststo clear

mean bed_beitrwende bed_innovtechn bed_finasp bed_verssich bed_beitrnetzst bed_infobera invest invest_coorientation, over(cluster3)
estat sd

lincom [bed_beitrwende]1 - [bed_beitrwende]2
lincom [bed_beitrwende]1 - [bed_beitrwende]3
lincom [bed_beitrwende]1 - [bed_beitrwende]4
lincom [bed_beitrwende]2 - [bed_beitrwende]3
lincom [bed_beitrwende]2 - [bed_beitrwende]4
lincom [bed_beitrwende]3 - [bed_beitrwende]4

lincom [bed_innovtechn]1 - [bed_innovtechn]2
lincom [bed_innovtechn]1 - [bed_innovtechn]3
lincom [bed_innovtechn]1 - [bed_innovtechn]4
lincom [bed_innovtechn]2 - [bed_innovtechn]3
lincom [bed_innovtechn]2 - [bed_innovtechn]4
lincom [bed_innovtechn]3 - [bed_innovtechn]4

lincom [bed_finasp]1 - [bed_finasp]2
lincom [bed_finasp]1 - [bed_finasp]3
lincom [bed_finasp]1 - [bed_finasp]4
lincom [bed_finasp]2 - [bed_finasp]3
lincom [bed_finasp]2 - [bed_finasp]4
lincom [bed_finasp]3 - [bed_finasp]4

lincom [bed_verssich]1 - [bed_verssich]2
lincom [bed_verssich]1 - [bed_verssich]3
lincom [bed_verssich]1 - [bed_verssich]4
lincom [bed_verssich]2 - [bed_verssich]3
lincom [bed_verssich]2 - [bed_verssich]4
lincom [bed_verssich]3 - [bed_verssich]4

lincom [bed_beitrnetzst]1 - [bed_beitrnetzst]2
lincom [bed_beitrnetzst]1 - [bed_beitrnetzst]3
lincom [bed_beitrnetzst]1 - [bed_beitrnetzst]4
lincom [bed_beitrnetzst]2 - [bed_beitrnetzst]3
lincom [bed_beitrnetzst]2 - [bed_beitrnetzst]4
lincom [bed_beitrnetzst]3 - [bed_beitrnetzst]4

lincom [bed_infobera]1 - [bed_infobera]2
lincom [bed_infobera]1 - [bed_infobera]3
lincom [bed_infobera]1 - [bed_infobera]4
lincom [bed_infobera]2 - [bed_infobera]3
lincom [bed_infobera]2 - [bed_infobera]4
lincom [bed_infobera]3 - [bed_infobera]4

lincom [invest]1 - [invest]2
lincom [invest]1 - [invest]3
lincom [invest]1 - [invest]4
lincom [invest]2 - [invest]3
lincom [invest]2 - [invest]4
lincom [invest]3 - [invest]4

lincom [invest_coorientation]1 - [invest_coorientation]2
lincom [invest_coorientation]1 - [invest_coorientation]3
lincom [invest_coorientation]1 - [invest_coorientation]4
lincom [invest_coorientation]2 - [invest_coorientation]3
lincom [invest_coorientation]2 - [invest_coorientation]4
lincom [invest_coorientation]3 - [invest_coorientation]4


//met --> 77 für jedes einzeln ausschließen!
eststo: estpost tabstat akterfuellt_beitrwende akterfuellt_innovtechn akterfuellt_finasp akterfuellt_verssich akterfuellt_beitrnetzst akterfuellt_infobera if akterfuellt_beitrwende<77 & akterfuellt_innovtechn<77 & akterfuellt_finasp<77 & akterfuellt_verssich<77 & akterfuellt_beitrnetzst<77 & akterfuellt_infobera<77, by (cluster3) stats(mean sd n)
esttab using Descr_Purchasemet2.rtf, cells ("akterfuellt_beitrwende(fmt(2)) akterfuellt_innovtechn(fmt(2)) akterfuellt_finasp(fmt(2)) akterfuellt_verssich(fmt(2)) akterfuellt_beitrnetzst(fmt(2)) akterfuellt_infobera(fmt(2))")
eststo clear

mean akterfuellt_beitrwende akterfuellt_innovtechn akterfuellt_finasp akterfuellt_verssich akterfuellt_beitrnetzst akterfuellt_infobera , over(cluster3)
estat sd


mean akterfuellt_beitrwende if akterfuellt_beitrwende<7, over(cluster3)
estat sd
lincom [akterfuellt_beitrwende]1 - [akterfuellt_beitrwende]2
lincom [akterfuellt_beitrwende]1 - [akterfuellt_beitrwende]3
lincom [akterfuellt_beitrwende]1 - [akterfuellt_beitrwende]4
lincom [akterfuellt_beitrwende]2 - [akterfuellt_beitrwende]3
lincom [akterfuellt_beitrwende]2 - [akterfuellt_beitrwende]4
lincom [akterfuellt_beitrwende]3 - [akterfuellt_beitrwende]4
tab akterfuellt_beitrwende cluster3 if akterfuellt_beitrwende<7
mean akterfuellt_beitrwende if akterfuellt_beitrwende<7
estat sd

mean akterfuellt_innovtechn if akterfuellt_innovtechn<7, over(cluster3)
estat sd
lincom [akterfuellt_innovtechn]1 - [akterfuellt_innovtechn]2
lincom [akterfuellt_innovtechn]1 - [akterfuellt_innovtechn]3
lincom [akterfuellt_innovtechn]1 - [akterfuellt_innovtechn]4
lincom [akterfuellt_innovtechn]2 - [akterfuellt_innovtechn]3
lincom [akterfuellt_innovtechn]2 - [akterfuellt_innovtechn]4
lincom [akterfuellt_innovtechn]3 - [akterfuellt_innovtechn]4
tab akterfuellt_innovtechn cluster3 if akterfuellt_innovtechn<7
mean akterfuellt_innovtechn if akterfuellt_innovtechn<7
estat sd

mean akterfuellt_finasp if akterfuellt_finasp<7, over(cluster3)
estat sd
lincom [akterfuellt_finasp]1 - [akterfuellt_finasp]2
lincom [akterfuellt_finasp]1 - [akterfuellt_finasp]3
lincom [akterfuellt_finasp]1 - [akterfuellt_finasp]4
lincom [akterfuellt_finasp]2 - [akterfuellt_finasp]3
lincom [akterfuellt_finasp]2 - [akterfuellt_finasp]4
lincom [akterfuellt_finasp]3 - [akterfuellt_finasp]4
tab akterfuellt_finasp cluster3 if akterfuellt_finasp<7
mean akterfuellt_finasp if akterfuellt_finasp<7
estat sd

mean akterfuellt_verssich if akterfuellt_verssich<7, over(cluster3)
estat sd
lincom [akterfuellt_verssich]1 - [akterfuellt_verssich]2
lincom [akterfuellt_verssich]1 - [akterfuellt_verssich]3
lincom [akterfuellt_verssich]1 - [akterfuellt_verssich]4
lincom [akterfuellt_verssich]2 - [akterfuellt_verssich]3
lincom [akterfuellt_verssich]2 - [akterfuellt_verssich]4
lincom [akterfuellt_verssich]3 - [akterfuellt_verssich]4
tab akterfuellt_verssich cluster3 if akterfuellt_verssich<7
mean akterfuellt_verssich if akterfuellt_verssich<7
estat sd

mean akterfuellt_beitrnetzst if akterfuellt_beitrnetzst<7, over(cluster3)
estat sd
lincom [akterfuellt_beitrnetzst]1 - [akterfuellt_beitrnetzst]2
lincom [akterfuellt_beitrnetzst]1 - [akterfuellt_beitrnetzst]3
lincom [akterfuellt_beitrnetzst]1 - [akterfuellt_beitrnetzst]4
lincom [akterfuellt_beitrnetzst]2 - [akterfuellt_beitrnetzst]3
lincom [akterfuellt_beitrnetzst]2 - [akterfuellt_beitrnetzst]4
lincom [akterfuellt_beitrnetzst]3 - [akterfuellt_beitrnetzst]4
tab akterfuellt_beitrnetzst cluster3 if akterfuellt_beitrnetzst<7
mean akterfuellt_beitrnetzst if akterfuellt_beitrnetzst<7
estat sd

mean akterfuellt_infobera if akterfuellt_infobera<7, over(cluster3)
estat sd
lincom [akterfuellt_infobera]1 - [akterfuellt_infobera]2
lincom [akterfuellt_infobera]1 - [akterfuellt_infobera]3
lincom [akterfuellt_infobera]1 - [akterfuellt_infobera]4
lincom [akterfuellt_infobera]2 - [akterfuellt_infobera]3
lincom [akterfuellt_infobera]2 - [akterfuellt_infobera]4
lincom [akterfuellt_infobera]3 - [akterfuellt_infobera]4
tab akterfuellt_infobera cluster3 if akterfuellt_infobera<7
mean akterfuellt_infobera if akterfuellt_infobera<7
estat sd


//Barriers --> 77 für jedes ausschließen
eststo: estpost tabstat problasp_unsicherheit problasp_ausgereift problasp_platzbedarf problasp_zeitrainvest problasp_datenschutz problasp_privatsph problasp_komplexitaet, by (cluster3) stats(mean sd n)
esttab using Descr_Barriers.rtf, cells ("problasp_unsicherheit(fmt(2)) problasp_ausgereift(fmt(2)) problasp_platzbedarf(fmt(2)) problasp_zeitrainvest(fmt(2)) problasp_datenschutz(fmt(2)) problasp_privatsph(fmt(2)) problasp_komplexitaet(fmt(2))")
eststo clear

mean problasp_unsicherheit if problasp_unsicherheit<7, over(cluster3)
estat sd
lincom [problasp_unsicherheit]1 - [problasp_unsicherheit]2
lincom [problasp_unsicherheit]1 - [problasp_unsicherheit]3
lincom [problasp_unsicherheit]1 - [problasp_unsicherheit]4
lincom [problasp_unsicherheit]2 - [problasp_unsicherheit]3
lincom [problasp_unsicherheit]2 - [problasp_unsicherheit]4
lincom [problasp_unsicherheit]3 - [problasp_unsicherheit]4
tab problasp_unsicherheit cluster3 if problasp_unsicherheit<7
mean problasp_unsicherheit if problasp_unsicherheit<7
estat sd

mean problasp_ausgereift if problasp_ausgereift<7, over(cluster3)
estat sd
lincom [problasp_ausgereift]1 - [problasp_ausgereift]2
lincom [problasp_ausgereift]1 - [problasp_ausgereift]3
lincom [problasp_ausgereift]1 - [problasp_ausgereift]4
lincom [problasp_ausgereift]2 - [problasp_ausgereift]3
lincom [problasp_ausgereift]2 - [problasp_ausgereift]4
lincom [problasp_ausgereift]3 - [problasp_ausgereift]4
tab problasp_ausgereift cluster3 if problasp_ausgereift<7
mean problasp_ausgereift if problasp_ausgereift<7
estat sd

mean problasp_platzbedarf if problasp_platzbedarf<7, over(cluster3)
estat sd
lincom [problasp_platzbedarf]1 - [problasp_platzbedarf]2
lincom [problasp_platzbedarf]1 - [problasp_platzbedarf]3
lincom [problasp_platzbedarf]1 - [problasp_platzbedarf]4
lincom [problasp_platzbedarf]2 - [problasp_platzbedarf]3
lincom [problasp_platzbedarf]2 - [problasp_platzbedarf]4
lincom [problasp_platzbedarf]3 - [problasp_platzbedarf]4
tab problasp_platzbedarf cluster3 if problasp_platzbedarf<7
mean problasp_platzbedarf if problasp_platzbedarf<7
estat sd

mean problasp_zeitrainvest if problasp_zeitrainvest<7, over(cluster3)
estat sd
lincom [problasp_zeitrainvest]1 - [problasp_zeitrainvest]2
lincom [problasp_zeitrainvest]1 - [problasp_zeitrainvest]3
lincom [problasp_zeitrainvest]1 - [problasp_zeitrainvest]4
lincom [problasp_zeitrainvest]2 - [problasp_zeitrainvest]3
lincom [problasp_zeitrainvest]2 - [problasp_zeitrainvest]4
lincom [problasp_zeitrainvest]3 - [problasp_zeitrainvest]4
tab problasp_zeitrainvest cluster3 if problasp_zeitrainvest<7
mean problasp_zeitrainvest if problasp_zeitrainvest<7
estat sd

mean problasp_datenschutz if problasp_datenschutz<7, over(cluster3)
estat sd
lincom [problasp_datenschutz]1 - [problasp_datenschutz]2
lincom [problasp_datenschutz]1 - [problasp_datenschutz]3
lincom [problasp_datenschutz]1 - [problasp_datenschutz]4
lincom [problasp_datenschutz]2 - [problasp_datenschutz]3
lincom [problasp_datenschutz]2 - [problasp_datenschutz]4
lincom [problasp_datenschutz]3 - [problasp_datenschutz]4
tab problasp_datenschutz cluster3 if problasp_datenschutz<7
mean problasp_datenschutz if problasp_datenschutz<7
estat sd

mean problasp_privatsph if problasp_privatsph<7, over(cluster3)
estat sd
lincom [problasp_privatsph]1 - [problasp_privatsph]2
lincom [problasp_privatsph]1 - [problasp_privatsph]3
lincom [problasp_privatsph]1 - [problasp_privatsph]4
lincom [problasp_privatsph]2 - [problasp_privatsph]3
lincom [problasp_privatsph]2 - [problasp_privatsph]4
lincom [problasp_privatsph]3 - [problasp_privatsph]4
tab problasp_privatsph cluster3 if problasp_privatsph<7
mean problasp_privatsph if problasp_privatsph<7
estat sd

mean problasp_komplexitaet if problasp_komplexitaet<7, over(cluster3)
estat sd
lincom [problasp_komplexitaet]1 - [problasp_komplexitaet]2
lincom [problasp_komplexitaet]1 - [problasp_komplexitaet]3
lincom [problasp_komplexitaet]1 - [problasp_komplexitaet]4
lincom [problasp_komplexitaet]2 - [problasp_komplexitaet]3
lincom [problasp_komplexitaet]2 - [problasp_komplexitaet]4
lincom [problasp_komplexitaet]3 - [problasp_komplexitaet]4
tab problasp_komplexitaet cluster3 if problasp_komplexitaet<7
mean problasp_komplexitaet if problasp_komplexitaet<7
estat sd
